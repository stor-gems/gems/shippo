# Shippo::TransactionRequestBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**async** | **BOOLEAN** |  | [optional] 
**label_file_type** | **String** |  | [optional] 
**metadata** | **String** |  | [optional] 
**rate** | **String** |  | 

