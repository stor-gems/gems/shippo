# Shippo::CustomsDeclarationsApi

All URIs are relative to *https://platform-api.goshippo.com/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_customs_declaration**](CustomsDeclarationsApi.md#create_customs_declaration) | **POST** /merchants/{MerchantId}/customs/declarations/ | Create a new customs declaration
[**list_customs_declarations**](CustomsDeclarationsApi.md#list_customs_declarations) | **GET** /merchants/{MerchantId}/customs/declarations/ | List all customs declarations
[**get_customs_declaration**](CustomsDeclarationsApi.md#get_customs_declaration) | **GET** /merchants/{MerchantId}/customs/declarations/{CustomsDeclarationId}/ | Retrieve a customs declaration

# **create_customs_declaration**
> CustomsDeclaration create_customs_declaration(merchant_id, opts)

Create a new customs declaration

Creates a new customs declaration object

### Example
```ruby
# load the gem
require 'shippo'
# setup authorization
Shippo.configure do |config|
  # Configure API key authorization: APIKeyHeader
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = Shippo::CustomsDeclarationsApi.new
merchant_id = 'merchant_id_example' # String | description: Object ID of the merchant
opts = { 
  body: Shippo::CreateCustomsDeclaration.new # CreateCustomsDeclaration | CustomsDeclaration details.
  shippo_api_version: 'shippo_api_version_example' # String | description: String used to pick a non-default api version to use
}

begin
  #Create a new customs declaration
  result = api_instance.create_customs_declaration(merchant_id, opts)
  p result
rescue Shippo::ApiError => e
  puts "Exception when calling CustomsDeclarationsApi->create_customs_declaration: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **merchant_id** | **String**| description: Object ID of the merchant | 
 **body** | [**CreateCustomsDeclaration**](CreateCustomsDeclaration.md)| CustomsDeclaration details. | [optional] 
 **shippo_api_version** | **String**| description: String used to pick a non-default api version to use | [optional] 

### Return type

[**CustomsDeclaration**](CustomsDeclaration.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **list_customs_declarations**
> PaginatedCustomsDeclarationsResponse list_customs_declarations(merchant_id, opts)

List all customs declarations

List all customs declaration objects

### Example
```ruby
# load the gem
require 'shippo'
# setup authorization
Shippo.configure do |config|
  # Configure API key authorization: APIKeyHeader
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = Shippo::CustomsDeclarationsApi.new
merchant_id = 'merchant_id_example' # String | description: Object ID of the merchant
opts = { 
  shippo_api_version: 'shippo_api_version_example', # String | description: String used to pick a non-default api version to use
  page: 1, # Integer | description: The page number you want to select
  results: 25 # Integer | description: The number of results to return per page (max 100)
}

begin
  #List all customs declarations
  result = api_instance.list_customs_declarations(merchant_id, opts)
  p result
rescue Shippo::ApiError => e
  puts "Exception when calling CustomsDeclarationsApi->list_customs_declarations: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **merchant_id** | **String**| description: Object ID of the merchant | 
 **shippo_api_version** | **String**| description: String used to pick a non-default api version to use | [optional] 
 **page** | **Integer**| description: The page number you want to select | [optional] [default to 1]
 **results** | **Integer**| description: The number of results to return per page (max 100) | [optional] [default to 25]

### Return type

[**PaginatedCustomsDeclarationsResponse**](PaginatedCustomsDeclarationsResponse.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **get_customs_declaration**
> CustomsDeclaration get_customs_declaration(customs_declaration_id, merchant_id, opts)

Retrieve a customs declaration

Retrieve an existing customs declaration by object id

### Example
```ruby
# load the gem
require 'shippo'
# setup authorization
Shippo.configure do |config|
  # Configure API key authorization: APIKeyHeader
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = Shippo::CustomsDeclarationsApi.new
customs_declaration_id = 'customs_declaration_id_example' # String | description: Object ID of the customs declaration
merchant_id = 'merchant_id_example' # String | description: Object ID of the merchant
opts = { 
  shippo_api_version: 'shippo_api_version_example', # String | description: String used to pick a non-default api version to use
  page: 1 # Integer | description: The page number you want to select
}

begin
  #Retrieve a customs declaration
  result = api_instance.get_customs_declaration(customs_declaration_id, merchant_id, opts)
  p result
rescue Shippo::ApiError => e
  puts "Exception when calling CustomsDeclarationsApi->get_customs_declaration: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customs_declaration_id** | **String**| description: Object ID of the customs declaration | 
 **merchant_id** | **String**| description: Object ID of the merchant | 
 **shippo_api_version** | **String**| description: String used to pick a non-default api version to use | [optional] 
 **page** | **Integer**| description: The page number you want to select | [optional] [default to 1]

### Return type

[**CustomsDeclaration**](CustomsDeclaration.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



