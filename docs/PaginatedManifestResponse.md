# Shippo::PaginatedManifestResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_next** | **String** |  | [optional] 
**previous** | **String** |  | [optional] 
**results** | [**Array&lt;Manifest&gt;**](Manifest.md) |  | [optional] 

