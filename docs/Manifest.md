# Shippo::Manifest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address_from** | **String** |  | 
**carrier_account** | **String** |  | 
**documents** | **Array&lt;String&gt;** |  | 
**object_created** | **DateTime** |  | 
**object_id** | **String** |  | 
**object_owner** | **String** |  | 
**object_updated** | **DateTime** |  | 
**shipment_date** | **String** |  | 
**status** | **String** |  | 
**transactions** | **Array&lt;String&gt;** |  | [optional] 

