# Shippo::ShipmentInsertRequestBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address_from** | [**AddressRequestBody**](AddressRequestBody.md) |  | 
**address_return** | [**AddressRequestBody**](AddressRequestBody.md) |  | [optional] 
**address_to** | [**AddressRequestBody**](AddressRequestBody.md) |  | 
**async** | **BOOLEAN** |  | [optional] 
**customs_declaration** | **String** |  | [optional] 
**extra** | [**Extra**](Extra.md) |  | [optional] 
**metadata** | **String** |  | [optional] 
**parcels** | [**Array&lt;ParcelCreateRequest&gt;**](ParcelCreateRequest.md) |  | 
**shipment_date** | **String** |  | [optional] 

