# Shippo::AllUserParcelTemplateResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**results** | [**Array&lt;UserParcelTemplateStruct&gt;**](UserParcelTemplateStruct.md) |  | [optional] 

