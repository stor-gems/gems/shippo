# Shippo::CarrierRegisterStatusResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**carrier_account** | **String** |  | [optional] 
**object_created** | **DateTime** |  | [optional] 
**object_owner** | **String** |  | [optional] 
**object_updated** | **DateTime** |  | [optional] 
**status** | **String** |  | [optional] 

