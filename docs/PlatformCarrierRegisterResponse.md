# Shippo::PlatformCarrierRegisterResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_id** | **String** |  | [optional] 
**active** | **BOOLEAN** |  | [optional] 
**carrier** | **String** |  | [optional] 
**is_shippo_account** | **BOOLEAN** |  | [optional] 
**metadata** | **String** |  | [optional] 
**object_id** | **String** |  | [optional] 
**object_owner** | **String** |  | [optional] 
**parameters** | **Object** |  | [optional] 
**test** | **BOOLEAN** |  | [optional] 

