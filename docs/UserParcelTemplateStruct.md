# Shippo::UserParcelTemplateStruct

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**distance_unit** | **String** | The measure unit of the package dimensions in inches, cm, or mm | [optional] 
**height** | **String** | The height of the package, in units specified by the distance_unit attribute | [optional] 
**length** | **String** | The length of the package, in units specified by the distance_unit attribute | [optional] 
**name** | **String** | The name of the User Parcel Template | [optional] 
**object_created** | **DateTime** | Date and time of User Parcel Template creation | [optional] 
**object_id** | **String** | Unique identifier of the given User Parcel Template object | [optional] 
**object_owner** | **String** | Username of the user who created the User Parcel Template object | [optional] 
**object_updated** | **DateTime** | Date and time of last update on User Parcel Template | [optional] 
**template** | [**CarrierParcelTemplateStruct**](CarrierParcelTemplateStruct.md) |  | [optional] 
**weight** | **String** | The weight of the package, in units specified by the weight_unit attribute | [optional] 
**weight_unit** | **String** | The weight unit of the package, as lb, g, kg, or oz | [optional] 
**width** | **String** | The width of the package, in units specified by the distance_unit attribute | [optional] 

