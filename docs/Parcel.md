# Shippo::Parcel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**distance_unit** | **String** | Required if template is not specified | 
**extra** | [**ParcelExtra**](ParcelExtra.md) |  | [optional] 
**height** | **String** | Required if template is not specified | 
**length** | **String** | Required if template is not specified | 
**mass_unit** | **String** |  | 
**metadata** | **String** | A string of up to 100 characters that can be filled with any additional information you want to attach to the object. | [optional] 
**object_created** | **DateTime** | Date and time of Parcel creation. | [optional] 
**object_id** | **String** | Unique identifier of the given Parcel object. This ID is required to create a Shipment object. | [optional] 
**object_owner** | **String** | Username of the user who created the Parcel object. | [optional] 
**object_state** | **String** | A Parcel will only be valid when all required values have been sent and validated successfully. | [optional] 
**object_updated** | **DateTime** | Date and time of last Parcel update. Since you cannot update Parcels after they were created, this time stamp reflects the time when the Parcel was changed by Shippo&#x27;s systems for the last time, e.g., during sorting the dimensions given. | [optional] 
**template** | **String** | Description: If template is passed, length, width, height, and distance_unit are not required | [optional] 
**test** | **BOOLEAN** | Indicates whether the object has been created in test mode. | [optional] 
**weight** | **String** |  | 
**width** | **String** | Required if template is not specified | 

