# Shippo::PlatformCarrierRegistration400Response

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**error** | **String** | This is just an example, this validation won&#x27;t be performed on USPS | [optional] 

