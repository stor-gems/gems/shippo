# Shippo::ParcelTemplate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**distance_unit** | **String** |  | 
**height** | **String** |  | 
**is_default** | **BOOLEAN** | Description: Can be null or can pass in a provider parcel token | [optional] 
**length** | **String** |  | 
**mass_unit** | **String** |  | 
**name** | **String** |  | [optional] 
**object_id** | **String** |  | [optional] 
**template** | **String** | Description: If template is passed, length, width, height, and distance_unit are not required | [optional] 
**weight** | **String** |  | 
**width** | **String** |  | 

