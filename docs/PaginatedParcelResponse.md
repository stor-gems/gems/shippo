# Shippo::PaginatedParcelResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_next** | **String** |  | [optional] 
**previous** | **String** |  | [optional] 
**results** | [**Array&lt;Parcel&gt;**](Parcel.md) |  | [optional] 

