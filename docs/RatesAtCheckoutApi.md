# Shippo::RatesAtCheckoutApi

All URIs are relative to *https://platform-api.goshippo.com/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**rates_at_checkout_create**](RatesAtCheckoutApi.md#rates_at_checkout_create) | **POST** /merchants/{MerchantId}/live-rates | Generate a live rates request
[**rates_at_checkout_default_parcel_delete**](RatesAtCheckoutApi.md#rates_at_checkout_default_parcel_delete) | **DELETE** /merchants/{MerchantId}/live-rates/settings/parcel-template | Clear current default parcel template
[**rates_at_checkout_default_parcel_list**](RatesAtCheckoutApi.md#rates_at_checkout_default_parcel_list) | **GET** /merchants/{MerchantId}/live-rates/settings/parcel-template | Show current default parcel template
[**rates_at_checkout_default_parcel_update**](RatesAtCheckoutApi.md#rates_at_checkout_default_parcel_update) | **PUT** /merchants/{MerchantId}/live-rates/settings/parcel-template | Update default parcel template

# **rates_at_checkout_create**
> CreateLiveRateResponse rates_at_checkout_create(opts)

Generate a live rates request

Initiates a live rates request. Include either the object ID for an existing Address record or a fully formed Address object when entering an address value. You can also enter the object ID of an existing User Parcel Template or a fully formed User Parcel Template object as the parcel value.

### Example
```ruby
# load the gem
require 'shippo'
# setup authorization
Shippo.configure do |config|
  # Configure API key authorization: APIKeyHeader
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = Shippo::RatesAtCheckoutApi.new
opts = { 
  body: Shippo::CreateLiveRateRequest.new # CreateLiveRateRequest | Generate rates at checkout
}

begin
  #Generate a live rates request
  result = api_instance.rates_at_checkout_create(opts)
  p result
rescue Shippo::ApiError => e
  puts "Exception when calling RatesAtCheckoutApi->rates_at_checkout_create: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**CreateLiveRateRequest**](CreateLiveRateRequest.md)| Generate rates at checkout | [optional] 

### Return type

[**CreateLiveRateResponse**](CreateLiveRateResponse.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **rates_at_checkout_default_parcel_delete**
> rates_at_checkout_default_parcel_delete

Clear current default parcel template

Clear the currently configured default parcel template for live rates.

### Example
```ruby
# load the gem
require 'shippo'
# setup authorization
Shippo.configure do |config|
  # Configure API key authorization: APIKeyHeader
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = Shippo::RatesAtCheckoutApi.new

begin
  #Clear current default parcel template
  api_instance.rates_at_checkout_default_parcel_delete
rescue Shippo::ApiError => e
  puts "Exception when calling RatesAtCheckoutApi->rates_at_checkout_default_parcel_delete: #{e}"
end
```

### Parameters
This endpoint does not need any parameter.

### Return type

nil (empty response body)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined



# **rates_at_checkout_default_parcel_list**
> GetLiveRateResponse rates_at_checkout_default_parcel_list

Show current default parcel template

Retrieve and display the currently configured default parcel template for live rates.

### Example
```ruby
# load the gem
require 'shippo'
# setup authorization
Shippo.configure do |config|
  # Configure API key authorization: APIKeyHeader
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = Shippo::RatesAtCheckoutApi.new

begin
  #Show current default parcel template
  result = api_instance.rates_at_checkout_default_parcel_list
  p result
rescue Shippo::ApiError => e
  puts "Exception when calling RatesAtCheckoutApi->rates_at_checkout_default_parcel_list: #{e}"
end
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**GetLiveRateResponse**](GetLiveRateResponse.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **rates_at_checkout_default_parcel_update**
> GetLiveRateResponse rates_at_checkout_default_parcel_update(opts)

Update default parcel template

Update the currently configured default parcel template for live rates. The object_id in the request payload should identify the user parcel template to be the new default.

### Example
```ruby
# load the gem
require 'shippo'
# setup authorization
Shippo.configure do |config|
  # Configure API key authorization: APIKeyHeader
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = Shippo::RatesAtCheckoutApi.new
opts = { 
  body: Shippo::DefaultParcelTemplate.new # DefaultParcelTemplate | Generate rates at checkout
}

begin
  #Update default parcel template
  result = api_instance.rates_at_checkout_default_parcel_update(opts)
  p result
rescue Shippo::ApiError => e
  puts "Exception when calling RatesAtCheckoutApi->rates_at_checkout_default_parcel_update: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**DefaultParcelTemplate**](DefaultParcelTemplate.md)| Generate rates at checkout | [optional] 

### Return type

[**GetLiveRateResponse**](GetLiveRateResponse.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



