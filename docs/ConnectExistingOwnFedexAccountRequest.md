# Shippo::ConnectExistingOwnFedexAccountRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_id** | **String** |  | 
**active** | **BOOLEAN** |  | [optional] 
**carrier** | **String** |  | 
**metadata** | **String** |  | [optional] 
**parameters** | [**FedexConnectExistingOwnAccountParameters**](FedexConnectExistingOwnAccountParameters.md) |  | 
**test** | **BOOLEAN** |  | [optional] 

