# Shippo::BasePickup

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**carrier_account** | **String** |  | 
**is_test** | **BOOLEAN** |  | [optional] 
**location** | [**Location**](Location.md) |  | 
**metadata** | **String** |  | [optional] 
**requested_end_time** | **DateTime** |  | 
**requested_start_time** | **DateTime** |  | 
**transactions** | **Array&lt;String&gt;** |  | 

