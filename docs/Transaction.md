# Shippo::Transaction

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**commercial_invoice_url** | **String** |  | [optional] 
**eta** | **String** |  | [optional] 
**label_url** | **String** |  | [optional] 
**messages** | **Array&lt;Object&gt;** |  | [optional] 
**metadata** | **String** |  | [optional] 
**object_created** | **DateTime** |  | [optional] 
**object_id** | **String** |  | [optional] 
**object_owner** | **String** |  | [optional] 
**object_state** | **String** |  | [optional] 
**object_updated** | **DateTime** |  | [optional] 
**qr_code_url** | **String** |  | [optional] 
**rate** | **String** |  | [optional] 
**status** | **String** |  | [optional] 
**test** | **BOOLEAN** |  | [optional] 
**tracking_number** | **String** |  | [optional] 
**tracking_status** | **String** |  | [optional] 
**tracking_url_provider** | **String** |  | [optional] 

