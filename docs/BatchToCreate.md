# Shippo::BatchToCreate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**batch_shipments** | [**Array&lt;BaseBatchShipment&gt;**](BaseBatchShipment.md) | Array of BatchShipment objects. The response keeps the same order as in the request array. | 
**default_carrier_account** | **String** | ID of the Carrier Account object to use as the default for all shipments in this Batch. The carrier account can be changed on a per-shipment basis by changing the carrier_account in the corresponding BatchShipment object. | 
**default_servicelevel_token** | **String** | Token of the service level to use as the default for all shipments in this Batch. The servicelevel can be changed on a per-shipment basis by changing the servicelevel_token in the corresponding BatchShipment object. | 
**label_filetype** | **String** | Print format of the label. If empty, will use the default format. | [optional] 
**metadata** | **String** | A string of up to 100 characters that can be filled with any additional information you want to attach to the object. | [optional] 

