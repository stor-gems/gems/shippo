# Shippo::CreateLiveRateResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **Integer** |  | [optional] 
**_next** | **String** |  | [optional] 
**previous** | **String** |  | [optional] 
**results** | [**Array&lt;LiveRate&gt;**](LiveRate.md) |  | [optional] 

