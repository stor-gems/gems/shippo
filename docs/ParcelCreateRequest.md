# Shippo::ParcelCreateRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**distance_unit** | **String** | Required if template is not specified | 
**height** | **String** | Required if template is not specified | 
**length** | **String** | Required if template is not specified | 
**mass_unit** | **String** |  | 
**template** | **String** | Description: If template is passed, length, width, height, and distance_unit are not required | [optional] 
**weight** | **String** |  | 
**width** | **String** | Required if template is not specified | 

