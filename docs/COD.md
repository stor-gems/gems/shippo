# Shippo::COD

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**amount** | **String** | Amount to be collected. | [optional] 
**currency** | **String** | Currency for the amount to be collected. Currently only USD is supported for FedEx and UPS. | [optional] 
**payment_method** | **String** | Secured funds include money orders, certified cheques and others (see UPS and FedEx for details). If no payment_method inputted the value defaults to \&quot;ANY\&quot;.) | [optional] 

