# Shippo::Rate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**amount** | **String** |  | 
**amount_local** | **String** |  | 
**arrives_by** | **Object** |  | [optional] 
**attributes** | **Array&lt;String&gt;** |  | 
**carrier_account** | **String** |  | 
**currency** | **String** |  | 
**currency_local** | **String** |  | 
**duration_terms** | **String** |  | [optional] 
**estimated_days** | **Integer** |  | [optional] 
**messages** | [**Array&lt;RateMessage&gt;**](RateMessage.md) |  | [optional] 
**object_created** | **DateTime** |  | 
**object_id** | **String** |  | 
**object_owner** | **String** |  | 
**provider** | **String** |  | 
**provider_image_75** | **String** |  | [optional] 
**provider_image_200** | **String** |  | [optional] 
**servicelevel** | [**ServiceLevel**](ServiceLevel.md) |  | 
**shipment** | **String** |  | 
**test** | **BOOLEAN** |  | [optional] 

