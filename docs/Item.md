# Shippo::Item

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**description** | **String** |  | 
**eccn_ear99** | **String** |  | [optional] 
**mass_unit** | **String** |  | 
**metadata** | **String** |  | [optional] 
**net_weight** | **String** |  | 
**origin_country** | **String** |  | 
**quantity** | **Integer** |  | 
**sku_code** | **String** | The stock keeping unit value of this item. | [optional] 
**tariff_number** | **String** |  | [optional] 
**value_amount** | **String** |  | 
**value_currency** | **String** |  | 

