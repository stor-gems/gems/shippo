# Shippo::ManifestsApi

All URIs are relative to *https://platform-api.goshippo.com/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_manifest**](ManifestsApi.md#create_manifest) | **POST** /merchants/{MerchantId}/manifests/ | Create a new manifest
[**get_manifest**](ManifestsApi.md#get_manifest) | **GET** /merchants/{MerchantId}/manifests/{ManifestId}/ | Retrieve a manifest
[**list_manifest**](ManifestsApi.md#list_manifest) | **GET** /merchants/{MerchantId}/manifests/ | List all manifests

# **create_manifest**
> Manifest create_manifest(merchant_id, opts)

Create a new manifest

Creates a new manifest object.

### Example
```ruby
# load the gem
require 'shippo'
# setup authorization
Shippo.configure do |config|
  # Configure API key authorization: APIKeyHeader
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = Shippo::ManifestsApi.new
merchant_id = 'merchant_id_example' # String | description: Object ID of the merchant
opts = { 
  body: Shippo::ManifestRequest.new # ManifestRequest | Manifest details and contact info.
  shippo_api_version: 'shippo_api_version_example' # String | description: String used to pick a non-default api version to use
}

begin
  #Create a new manifest
  result = api_instance.create_manifest(merchant_id, opts)
  p result
rescue Shippo::ApiError => e
  puts "Exception when calling ManifestsApi->create_manifest: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **merchant_id** | **String**| description: Object ID of the merchant | 
 **body** | [**ManifestRequest**](ManifestRequest.md)| Manifest details and contact info. | [optional] 
 **shippo_api_version** | **String**| description: String used to pick a non-default api version to use | [optional] 

### Return type

[**Manifest**](Manifest.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **get_manifest**
> Manifest get_manifest(merchant_id, manifest_id, opts)

Retrieve a manifest

Retrieve an existing manifest by object id.

### Example
```ruby
# load the gem
require 'shippo'
# setup authorization
Shippo.configure do |config|
  # Configure API key authorization: APIKeyHeader
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = Shippo::ManifestsApi.new
merchant_id = 'merchant_id_example' # String | description: Object ID of the merchant
manifest_id = 'manifest_id_example' # String | description: Object ID of the manifest to update
opts = { 
  shippo_api_version: 'shippo_api_version_example' # String | description: String used to pick a non-default api version to use
}

begin
  #Retrieve a manifest
  result = api_instance.get_manifest(merchant_id, manifest_id, opts)
  p result
rescue Shippo::ApiError => e
  puts "Exception when calling ManifestsApi->get_manifest: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **merchant_id** | **String**| description: Object ID of the merchant | 
 **manifest_id** | **String**| description: Object ID of the manifest to update | 
 **shippo_api_version** | **String**| description: String used to pick a non-default api version to use | [optional] 

### Return type

[**Manifest**](Manifest.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **list_manifest**
> PaginatedManifestResponse list_manifest(merchant_id, opts)

List all manifests

List all manifest objects.

### Example
```ruby
# load the gem
require 'shippo'
# setup authorization
Shippo.configure do |config|
  # Configure API key authorization: APIKeyHeader
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = Shippo::ManifestsApi.new
merchant_id = 'merchant_id_example' # String | description: Object ID of the merchant
opts = { 
  shippo_api_version: 'shippo_api_version_example', # String | description: String used to pick a non-default api version to use
  page: 1, # Integer | description: The page number you want to select
  results: 25 # Integer | description: The number of results to return per page (max 100)
}

begin
  #List all manifests
  result = api_instance.list_manifest(merchant_id, opts)
  p result
rescue Shippo::ApiError => e
  puts "Exception when calling ManifestsApi->list_manifest: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **merchant_id** | **String**| description: Object ID of the merchant | 
 **shippo_api_version** | **String**| description: String used to pick a non-default api version to use | [optional] 
 **page** | **Integer**| description: The page number you want to select | [optional] [default to 1]
 **results** | **Integer**| description: The number of results to return per page (max 100) | [optional] [default to 25]

### Return type

[**PaginatedManifestResponse**](PaginatedManifestResponse.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



