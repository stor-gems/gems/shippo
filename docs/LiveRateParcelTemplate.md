# Shippo::LiveRateParcelTemplate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**distance_unit** | **String** |  | [optional] 
**height** | **String** | The height of the package, in units specified by the distance_unit attribute | [optional] 
**length** | **String** | The length of the package, in units specified by the distance_unit attribute | [optional] 
**name** | **String** | The name of the User Parcel Template | [optional] 
**object_id** | **String** | Unique identifier of the given User Parcel Template object | [optional] 
**template** | **String** | The object representing the carrier parcel template, if the template was created from a preset carrier template. Otherwise null. | [optional] 
**weight** | **String** | The weight of the package, in units specified by the weight_unit attribute | [optional] 
**weight_unit** | **String** |  | [optional] 
**width** | **String** | The width of the package, in units specified by the distance_unit attribute | [optional] 

