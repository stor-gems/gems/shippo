# Shippo::LiveRate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**amount** | **String** | The value of the price for the service group, in units of currency of the sender address | [optional] 
**amount_local** | **String** | The value of the price for the service group, in the currency of the destination address | [optional] 
**currency** | **String** | The ISO 4217 currency code for the price | [optional] 
**currency_local** | **String** | The ISO 4217 currency code for the currency describing amount_local | [optional] 
**estimated_days** | **Integer** | The estimated days in transit of the rate that powers the shipping option, if available. | [optional] 
**title** | **String** | The name of the service group being returned | [optional] 

