# Shippo::BatchShipment

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**carrier_account** | **String** | Object ID of the carrier account to be used for this shipment (will override batch default) | [optional] 
**messages** | [**Array&lt;ErrorMessage&gt;**](ErrorMessage.md) | List of Shipment and Transaction error messages. | [optional] 
**metadata** | **String** | A string of up to 100 characters that can be filled with any additional information you want to attach to the object. | [optional] 
**object_id** | **String** | Object ID of this batch shipment. Can be used in the remove_shipments endpoint. | 
**servicelevel_token** | **String** | A token that sets the shipping method for the batch, overriding the batch default. | [optional] 
**shipment** | **String** | (string or object) Object ID of a previously created shipment, to be added to the batch. OR required fields of a new Shipment Object, to be created. | 
**status** | **String** | INVALID batch shipments cannot be purchased and will have to be removed, fixed, and added to the batch again. VALID batch shipments can be purchased. Batch shipments with the status TRANSACTION_FAILED were not able to be purchased and the error will be displayed on the message field. INCOMPLETE batch shipments have an issue with the Address and will need to be removed, fixed, and added to the batch again. | 
**transaction** | **String** | Object ID of the transaction object created for this batch shipment. | [optional] 

