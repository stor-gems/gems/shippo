# Shippo::MerchantsApi

All URIs are relative to *https://platform-api.goshippo.com/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**list_merchant**](MerchantsApi.md#list_merchant) | **GET** /merchants/ | List all merchants
[**get_merchant**](MerchantsApi.md#get_merchant) | **GET** /merchants/{MerchantId}/ | Retrieve a merchant
[**register_merchant**](MerchantsApi.md#register_merchant) | **POST** /merchants/ | Create a merchant
[**update_merchant**](MerchantsApi.md#update_merchant) | **PUT** /merchants/{MerchantId}/ | Update a merchant

# **list_merchant**
> PaginatedMerchantResponse list_merchant(opts)

List all merchants

List merchant objects

### Example
```ruby
# load the gem
require 'shippo'
# setup authorization
Shippo.configure do |config|
  # Configure API key authorization: APIKeyHeader
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = Shippo::MerchantsApi.new
opts = { 
  shippo_api_version: 'shippo_api_version_example', # String | description: String used to pick a non-default api version to use
  page: 1, # Integer | description: The page number you want to select
  results: 25 # Integer | description: The number of results to return per page (max 100)
}

begin
  #List all merchants
  result = api_instance.list_merchant(opts)
  p result
rescue Shippo::ApiError => e
  puts "Exception when calling MerchantsApi->list_merchant: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **shippo_api_version** | **String**| description: String used to pick a non-default api version to use | [optional] 
 **page** | **Integer**| description: The page number you want to select | [optional] [default to 1]
 **results** | **Integer**| description: The number of results to return per page (max 100) | [optional] [default to 25]

### Return type

[**PaginatedMerchantResponse**](PaginatedMerchantResponse.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **get_merchant**
> Merchant get_merchant(merchant_id, opts)

Retrieve a merchant

Retrieve a merchant object

### Example
```ruby
# load the gem
require 'shippo'
# setup authorization
Shippo.configure do |config|
  # Configure API key authorization: APIKeyHeader
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = Shippo::MerchantsApi.new
merchant_id = 'merchant_id_example' # String | description: Object ID of the merchant
opts = { 
  shippo_api_version: 'shippo_api_version_example' # String | description: String used to pick a non-default api version to use
}

begin
  #Retrieve a merchant
  result = api_instance.get_merchant(merchant_id, opts)
  p result
rescue Shippo::ApiError => e
  puts "Exception when calling MerchantsApi->get_merchant: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **merchant_id** | **String**| description: Object ID of the merchant | 
 **shippo_api_version** | **String**| description: String used to pick a non-default api version to use | [optional] 

### Return type

[**Merchant**](Merchant.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **register_merchant**
> Merchant register_merchant(opts)

Create a merchant

Creates a merchant object

### Example
```ruby
# load the gem
require 'shippo'
# setup authorization
Shippo.configure do |config|
  # Configure API key authorization: APIKeyHeader
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = Shippo::MerchantsApi.new
opts = { 
  body: Shippo::BaseMerchantStruct.new # BaseMerchantStruct | Merchant details and contact info.
  shippo_api_version: 'shippo_api_version_example' # String | description: String used to pick a non-default api version to use
}

begin
  #Create a merchant
  result = api_instance.register_merchant(opts)
  p result
rescue Shippo::ApiError => e
  puts "Exception when calling MerchantsApi->register_merchant: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**BaseMerchantStruct**](BaseMerchantStruct.md)| Merchant details and contact info. | [optional] 
 **shippo_api_version** | **String**| description: String used to pick a non-default api version to use | [optional] 

### Return type

[**Merchant**](Merchant.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **update_merchant**
> Merchant update_merchant(merchant_id, opts)

Update a merchant

Updates a merchant object

### Example
```ruby
# load the gem
require 'shippo'
# setup authorization
Shippo.configure do |config|
  # Configure API key authorization: APIKeyHeader
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = Shippo::MerchantsApi.new
merchant_id = 'merchant_id_example' # String | description: Object ID of the merchant
opts = { 
  body: Shippo::BaseMerchantStruct.new # BaseMerchantStruct | Merchant details and contact info.
  shippo_api_version: 'shippo_api_version_example' # String | description: String used to pick a non-default api version to use
}

begin
  #Update a merchant
  result = api_instance.update_merchant(merchant_id, opts)
  p result
rescue Shippo::ApiError => e
  puts "Exception when calling MerchantsApi->update_merchant: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **merchant_id** | **String**| description: Object ID of the merchant | 
 **body** | [**BaseMerchantStruct**](BaseMerchantStruct.md)| Merchant details and contact info. | [optional] 
 **shippo_api_version** | **String**| description: String used to pick a non-default api version to use | [optional] 

### Return type

[**Merchant**](Merchant.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



