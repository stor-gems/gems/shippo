# Shippo::CreateUPSMasterAccountRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**carrier** | **String** |  | 
**parameters** | [**UPSCreateMasterAccountParameters**](UPSCreateMasterAccountParameters.md) |  | [optional] 

