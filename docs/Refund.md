# Shippo::Refund

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**object_created** | **DateTime** |  | [optional] 
**object_id** | **String** |  | [optional] 
**object_owner** | **String** |  | [optional] 
**object_updated** | **DateTime** |  | [optional] 
**status** | **String** |  | [optional] 
**test** | **BOOLEAN** |  | [optional] 
**transaction** | **String** |  | [optional] 

