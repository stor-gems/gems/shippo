# Shippo::UPSConnectExistingOwnAccountParameters

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_number** | **String** | The UPS account number | 
**aia_country_iso2** | **String** | Only required if has_invoice is true. Country associated with the account that issued the invoice | [optional] 
**billing_address_city** | **String** |  | 
**billing_address_country_iso2** | **String** |  | 
**billing_address_state** | **String** |  | 
**billing_address_street1** | **String** |  | 
**billing_address_street2** | **String** | Empty string acceptable for billing_address_street2 | [optional] 
**billing_address_zip** | **String** |  | 
**collec_country_iso2** | **String** |  | 
**collec_zip** | **String** | Zip code of the collection/pickup address | 
**company** | **String** | Company name. Full name is acceptable in this field if the user has no company name | 
**currency_code** | **String** | Only required if has_invoice is true. 3-letter currency code associated with invoice_value | [optional] 
**email** | **String** |  | 
**full_name** | **String** |  | 
**has_invoice** | **BOOLEAN** | true if user has been issued a UPS invoice within the past 90 days for the US or Canada; and 45 days for any other countries. User can use data from any of the last 3 invoices | 
**invoice_controlid** | **String** | Only required if aia_country_iso2 is US and has_invoice is true. | [optional] 
**invoice_date** | **String** | Only required if has_invoice is true. Date the invoice was issued. yyyymmdd format | [optional] 
**invoice_number** | **String** |  | [optional] 
**invoice_value** | **String** | Only required if has_invoice is true. Max 16 digits before decimal and 2 digits after decimal | [optional] 
**phone** | **String** |  | 
**title** | **String** | User&#x27;s title, e.g. including but not limited to Manager, Doctor, Artist, Engineer, Mr, Ms, Mrs, Mx | 
**ups_agreements** | **BOOLEAN** | Whether the user agrees to the UPS terms and conditions or not. Error 400 will be returned if passed in as false | 

