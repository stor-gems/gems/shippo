# Shippo::CanadaPostCreateMasterAccountParameters

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**canada_post_terms** | **BOOLEAN** | Whether or not the user agrees to Canada Post&#x27;s terms. If passed in as false, request will fail with error 400 | 
**company** | **String** |  | 
**email** | **String** |  | 
**full_name** | **String** |  | 
**phone** | **String** | Needs to be a valid phone number and cannot be null | 

