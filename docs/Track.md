# Shippo::Track

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address_from** | [**TrackingStatusLocation**](TrackingStatusLocation.md) |  | 
**address_to** | [**TrackingStatusLocation**](TrackingStatusLocation.md) |  | 
**carrier** | **String** |  | 
**eta** | **DateTime** |  | 
**messages** | **Array&lt;String&gt;** |  | 
**metadata** | **String** |  | [optional] 
**original_eta** | **DateTime** |  | 
**servicelevel** | [**ServiceLevel**](ServiceLevel.md) |  | 
**tracking_history** | [**Array&lt;TrackingStatus&gt;**](TrackingStatus.md) |  | 
**tracking_number** | **String** |  | 
**tracking_status** | [**TrackingStatus**](TrackingStatus.md) |  | 
**transaction** | **String** |  | 

