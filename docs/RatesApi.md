# Shippo::RatesApi

All URIs are relative to *https://platform-api.goshippo.com/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**list_shipment_rates_cc**](RatesApi.md#list_shipment_rates_cc) | **GET** /merchants/{MerchantId}/shipments/{ShipmentId}/rates/{CurrencyCode}/ | Retrieve shipment rates in currency
[**list_shipment_rates**](RatesApi.md#list_shipment_rates) | **GET** /merchants/{MerchantId}/shipments/{ShipmentId}/rates/{CurrencyCode} | Retrieve shipment rates
[**get_rate**](RatesApi.md#get_rate) | **GET** /merchants/{MerchantId}/rates/{RateId}/ | Retrieve a rate

# **list_shipment_rates_cc**
> PaginatedRateResponse list_shipment_rates_cc(merchant_id, currency_code, shipment_id, opts)

Retrieve shipment rates in currency

Each valid shipment object will automatically trigger the calculation of all available rates. Depending on your shipment data, there may be none, one or multiple rates.  By default, the calculated Rates will return the price in two currencies under the amount and amount_local keys, respectively. The amount key will contain the price of a rate expressed in the currency that is used in the country from which parcel originates, and the amount_local key will contain the price expressed in the currency that is used in the country the parcel is shipped to. You can request rates with prices expressed in a different currency by adding the desired currency code in the end of the resource URL. The full list of supported currencies along with their codes can be viewed on [open exchange rates](http://openexchangerates.org/api/currencies.json).  Note: re-requesting the rates with a different currency code will re-queue the shipment (i.e. set the Shipment's status to QUEUED) and the converted currency rates will only be available when the Shipment's status is set to SUCCESS.

### Example
```ruby
# load the gem
require 'shippo'
# setup authorization
Shippo.configure do |config|
  # Configure API key authorization: APIKeyHeader
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = Shippo::RatesApi.new
merchant_id = 'merchant_id_example' # String | description: Object ID of the merchant
currency_code = 'currency_code_example' # String | description: ISO currency code for the rates
shipment_id = 'shipment_id_example' # String | description: Object ID of the shipment to update
opts = { 
  shippo_api_version: 'shippo_api_version_example', # String | description: String used to pick a non-default api version to use
  page: 1, # Integer | description: The page number you want to select
  results: 25 # Integer | description: The number of results to return per page (max 100)
}

begin
  #Retrieve shipment rates in currency
  result = api_instance.list_shipment_rates_cc(merchant_id, currency_code, shipment_id, opts)
  p result
rescue Shippo::ApiError => e
  puts "Exception when calling RatesApi->list_shipment_rates_cc: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **merchant_id** | **String**| description: Object ID of the merchant | 
 **currency_code** | **String**| description: ISO currency code for the rates | 
 **shipment_id** | **String**| description: Object ID of the shipment to update | 
 **shippo_api_version** | **String**| description: String used to pick a non-default api version to use | [optional] 
 **page** | **Integer**| description: The page number you want to select | [optional] [default to 1]
 **results** | **Integer**| description: The number of results to return per page (max 100) | [optional] [default to 25]

### Return type

[**PaginatedRateResponse**](PaginatedRateResponse.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **list_shipment_rates**
> PaginatedRateResponse list_shipment_rates(merchant_id, shipment_id, opts)

Retrieve shipment rates

Gets a paginated list of rates associated with a shipment

### Example
```ruby
# load the gem
require 'shippo'
# setup authorization
Shippo.configure do |config|
  # Configure API key authorization: APIKeyHeader
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = Shippo::RatesApi.new
merchant_id = 'merchant_id_example' # String | description: Object ID of the merchant
shipment_id = 'shipment_id_example' # String | description: Object ID of the shipment to update
opts = { 
  shippo_api_version: 'shippo_api_version_example', # String | description: String used to pick a non-default api version to use
  page: 1, # Integer | description: The page number you want to select
  results: 25 # Integer | description: The number of results to return per page (max 100)
}

begin
  #Retrieve shipment rates
  result = api_instance.list_shipment_rates(merchant_id, shipment_id, opts)
  p result
rescue Shippo::ApiError => e
  puts "Exception when calling RatesApi->list_shipment_rates: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **merchant_id** | **String**| description: Object ID of the merchant | 
 **shipment_id** | **String**| description: Object ID of the shipment to update | 
 **shippo_api_version** | **String**| description: String used to pick a non-default api version to use | [optional] 
 **page** | **Integer**| description: The page number you want to select | [optional] [default to 1]
 **results** | **Integer**| description: The number of results to return per page (max 100) | [optional] [default to 25]

### Return type

[**PaginatedRateResponse**](PaginatedRateResponse.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **get_rate**
> Rate get_rate(merchant_id, rate_id, opts)

Retrieve a rate

Retrieve an existing rate by object id for the merchant

### Example
```ruby
# load the gem
require 'shippo'
# setup authorization
Shippo.configure do |config|
  # Configure API key authorization: APIKeyHeader
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = Shippo::RatesApi.new
merchant_id = 'merchant_id_example' # String | description: Object ID of the merchant
rate_id = 'rate_id_example' # String | description: Object ID of the rate
opts = { 
  shippo_api_version: 'shippo_api_version_example' # String | description: String used to pick a non-default api version to use
}

begin
  #Retrieve a rate
  result = api_instance.get_rate(merchant_id, rate_id, opts)
  p result
rescue Shippo::ApiError => e
  puts "Exception when calling RatesApi->get_rate: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **merchant_id** | **String**| description: Object ID of the merchant | 
 **rate_id** | **String**| description: Object ID of the rate | 
 **shippo_api_version** | **String**| description: String used to pick a non-default api version to use | [optional] 

### Return type

[**Rate**](Rate.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



