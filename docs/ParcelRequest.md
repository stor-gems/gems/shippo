# Shippo::ParcelRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**distance_unit** | **String** | Required if template is not specified | 
**extra** | [**ParcelExtra**](ParcelExtra.md) |  | [optional] 
**height** | **String** | Required if template is not specified | 
**length** | **String** | Required if template is not specified | 
**mass_unit** | **String** |  | 
**metadata** | **String** | A string of up to 100 characters that can be filled with any additional information you want to attach to the object. | [optional] 
**template** | **String** | Description: If template is passed, length, width, height, and distance_unit are not required | [optional] 
**test** | **BOOLEAN** | Indicates whether the object has been created in test mode. | [optional] 
**weight** | **String** |  | 
**width** | **String** | Required if template is not specified | 

