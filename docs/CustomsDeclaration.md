# Shippo::CustomsDeclaration

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**aes_itn** | **String** | required if eel_pfc is &#x27;AES_ITN&#x27; | [optional] 
**b13a_filing_option** | **String** |  | [optional] 
**b13a_number** | **String** | must be provided if and only if b13a_filing_option is provided | [optional] 
**certificate** | **String** |  | [optional] 
**certify** | **BOOLEAN** |  | 
**certify_signer** | **String** |  | 
**commercial_invoice** | **String** |  | [optional] 
**contents_explanation** | **String** | required if contents_type is &#x27;OTHER&#x27; | [optional] 
**contents_type** | **String** |  | 
**disclaimer** | **String** |  | [optional] 
**eel_pfc** | **String** |  | [optional] 
**exporter_reference** | **String** |  | [optional] 
**importer_reference** | **String** |  | [optional] 
**incoterm** | **String** |  | [optional] 
**invoice** | **String** |  | [optional] 
**items** | **Array&lt;String&gt;** |  | 
**license** | **String** |  | [optional] 
**metadata** | **String** |  | [optional] 
**non_delivery_option** | **String** |  | 
**notes** | **String** |  | [optional] 
**object_created** | **DateTime** |  | [optional] 
**object_id** | **String** |  | [optional] 
**object_owner** | **String** |  | [optional] 
**object_state** | **String** |  | [optional] 
**object_updated** | **DateTime** |  | [optional] 
**test** | **BOOLEAN** |  | [optional] 

