# Shippo::CreateHermesUKMasterAccountRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**carrier** | **String** |  | 
**parameters** | **Object** |  | 

