# Shippo::TrackingStatus

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**location** | [**TrackingStatusLocation**](TrackingStatusLocation.md) |  | 
**object_created** | **DateTime** |  | 
**object_id** | **String** |  | 
**object_updated** | **DateTime** |  | 
**status** | **String** |  | 
**status_date** | **DateTime** |  | 
**status_details** | **String** |  | 

