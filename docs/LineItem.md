# Shippo::LineItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**currency** | **String** | Currency of the total_price amount. | [optional] 
**manufacture_country** | **String** | Country the item was manufactured in. In the Shippo dashboard, this value will be used ot pre-fill the customs declaration when creating a label for this order. | [optional] 
**max_delivery_time** | **DateTime** | The date and time this item needs to be delivered by, i.e. by when the carrier delivers it to the buyer. | [optional] 
**max_ship_time** | **DateTime** | The date and time this item needs to be fulfilled by, i.e. by when the shipping label needs to be created and handed over to the carrier. | [optional] 
**quantity** | **Integer** | The quantity of this item in this order. | [optional] 
**sku** | **String** | The stock keeping unit value of this item. | [optional] 
**title** | **String** | Title of the line item. | [optional] 
**total_price** | **String** | Total price paid by the buyer for this item (or these items, if quantity &gt; 1). | [optional] 
**variant_title** | **String** | A variant is a specific variation of an item (e.g. \&quot;size M\&quot; or \&quot;color blue\&quot;). Variants might be exposed as a separate resource in the future too. Currently the variant title is a free text field describing the variant. | [optional] 
**weight** | **String** | Total weight of this/these item(s). Instead of specifying the weight of all items, you can also set the total_weight value of the order object. | [optional] 
**weight_unit** | **String** | The unit used for the weight field | [optional] 

