# Shippo::Address

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**city** | **String** | required for purchase | [optional] 
**company** | **String** |  | [optional] 
**country** | **String** |  | 
**email** | **String** |  | [optional] 
**is_complete** | **BOOLEAN** |  | [optional] 
**is_residential** | **BOOLEAN** |  | [optional] 
**metadata** | **String** |  | [optional] 
**name** | **String** | required for purchase | [optional] 
**object_created** | **DateTime** |  | [optional] 
**object_id** | **String** |  | [optional] 
**object_owner** | **String** |  | [optional] 
**object_updated** | **DateTime** |  | [optional] 
**phone** | **String** |  | [optional] 
**state** | **String** | required for purchase for some countries | [optional] 
**street1** | **String** | required for purchase | [optional] 
**street2** | **String** |  | [optional] 
**validation_results** | [**AddressValidationResults**](AddressValidationResults.md) |  | [optional] 
**zip** | **String** | required for purchase | [optional] 

