# Shippo::BaseBatchShipment

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**carrier_account** | **String** | Object ID of the carrier account to be used for this shipment (will override batch default) | [optional] 
**metadata** | **String** | A string of up to 100 characters that can be filled with any additional information you want to attach to the object. | [optional] 
**servicelevel_token** | **String** | A token that sets the shipping method for the batch, overriding the batch default. | [optional] 
**shipment** | **String** | (string or object) Object ID of a previously created shipment, to be added to the batch. OR required fields of a new Shipment Object, to be created. | 

