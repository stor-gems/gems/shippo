# Shippo::BaseAddressStruct

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**city** | **String** |  | 
**country** | **String** |  | 
**email** | **String** |  | [optional] 
**name** | **String** |  | 
**phone** | **String** |  | [optional] 
**state** | **String** |  | 
**street1** | **String** |  | 
**zip** | **String** |  | 

