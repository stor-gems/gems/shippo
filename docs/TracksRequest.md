# Shippo::TracksRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**carrier** | **String** |  | 
**metadata** | **String** |  | [optional] 
**tracking_number** | **String** |  | 

