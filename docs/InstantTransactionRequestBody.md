# Shippo::InstantTransactionRequestBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**carrier_account** | **String** |  | 
**label_file_type** | **String** |  | [optional] 
**metadata** | **String** |  | [optional] 
**servicelevel_token** | **String** |  | 
**shipment** | [**ShipmentInsertRequestBody**](ShipmentInsertRequestBody.md) |  | 

