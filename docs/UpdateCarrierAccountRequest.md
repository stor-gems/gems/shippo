# Shippo::UpdateCarrierAccountRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_id** | **String** |  | 
**active** | **BOOLEAN** |  | [optional] 
**carrier** | **String** |  | 
**parameters** | **Object** |  | [optional] 

