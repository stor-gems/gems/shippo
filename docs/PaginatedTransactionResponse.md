# Shippo::PaginatedTransactionResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_next** | **String** |  | [optional] 
**previous** | **String** |  | [optional] 
**results** | [**Array&lt;Transaction&gt;**](Transaction.md) |  | [optional] 

