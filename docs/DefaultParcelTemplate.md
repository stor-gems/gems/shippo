# Shippo::DefaultParcelTemplate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**object_id** | **String** | Unique identifier of the given User Parcel Template object | 

