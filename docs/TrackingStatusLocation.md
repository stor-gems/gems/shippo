# Shippo::TrackingStatusLocation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**city** | **String** |  | [optional] 
**country** | **String** |  | [optional] 
**state** | **String** |  | [optional] 
**zip** | **String** |  | [optional] 

