# Shippo::AddressRequestBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**city** | **String** | required for purchase | [optional] 
**company** | **String** |  | [optional] 
**country** | **String** |  | 
**email** | **String** |  | [optional] 
**is_residential** | **BOOLEAN** |  | [optional] 
**metadata** | **String** |  | [optional] 
**name** | **String** | required for purchase | [optional] 
**phone** | **String** |  | [optional] 
**state** | **String** | required for purchase for some countries | [optional] 
**street1** | **String** | required for purchase | [optional] 
**street2** | **String** |  | [optional] 
**validate** | **BOOLEAN** |  | [optional] 
**zip** | **String** | required for purchase | [optional] 

