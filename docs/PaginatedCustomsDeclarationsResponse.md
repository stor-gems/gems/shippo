# Shippo::PaginatedCustomsDeclarationsResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_next** | **String** |  | [optional] 
**previous** | **String** |  | [optional] 
**results** | [**Array&lt;CustomsDeclaration&gt;**](CustomsDeclaration.md) |  | [optional] 

