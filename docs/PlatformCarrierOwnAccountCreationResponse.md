# Shippo::PlatformCarrierOwnAccountCreationResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_id** | **String** | Some fields with sensitive data may returned masked in the response, e.g. \&quot;****\&quot; | [optional] 
**active** | **BOOLEAN** |  | [optional] 
**carrier** | **String** |  | [optional] 
**is_shippo_account** | **BOOLEAN** |  | [optional] 
**metadata** | **String** |  | [optional] 
**object_id** | **String** |  | [optional] 
**object_owner** | **String** |  | [optional] 
**parameters** | [**UPSConnectExistingOwnAccountParameters**](UPSConnectExistingOwnAccountParameters.md) |  | [optional] 
**test** | **BOOLEAN** |  | [optional] 

