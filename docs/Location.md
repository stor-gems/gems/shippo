# Shippo::Location

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address** | [**BaseAddressStruct**](BaseAddressStruct.md) |  | 
**building_location_type** | **String** |  | 
**building_type** | **String** |  | [optional] 
**instructions** | **String** |  | [optional] 

