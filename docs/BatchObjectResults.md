# Shippo::BatchObjectResults

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**creation_failed** | **Integer** |  | 
**creation_succeeded** | **Integer** |  | 
**purchase_failed** | **Integer** |  | 
**purchase_succeeded** | **Integer** |  | 

