# Shippo::ParcelInsurance

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**amount** | **String** | Amount to be collected. | [optional] 
**content** | **String** | Specify package content for insurance. | [optional] 
**currency** | **String** | Currency for the amount to be collected. Currently only USD is supported for FedEx and UPS. | [optional] 
**provider** | **String** | Specify the carrier insurance to have Insurance provided by the carrier directly. Currently only FedEx and UPS are supported. | [optional] 

