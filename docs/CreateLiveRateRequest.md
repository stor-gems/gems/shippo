# Shippo::CreateLiveRateRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address_from** | [**BaseAddressStruct**](BaseAddressStruct.md) |  | [optional] 
**address_to** | [**BaseAddressStruct**](BaseAddressStruct.md) |  | 
**line_items** | [**Array&lt;LiveRateLineItem&gt;**](LiveRateLineItem.md) | Array of Line Item objects | 
**parcel** | [**Array&lt;Parcel&gt;**](Parcel.md) | Fully formed Parcel object OR the object ID for an existing User Parcel Template | [optional] 

