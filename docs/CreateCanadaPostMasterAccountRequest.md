# Shippo::CreateCanadaPostMasterAccountRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**carrier** | **String** |  | 
**parameters** | [**CanadaPostCreateMasterAccountParameters**](CanadaPostCreateMasterAccountParameters.md) |  | 

