# Shippo::Merchant

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | **String** |  | 
**first_name** | **String** |  | 
**last_name** | **String** |  | 
**merchant_name** | **String** |  | 
**object_created** | **DateTime** |  | [optional] 
**object_id** | **String** |  | [optional] 
**object_updated** | **DateTime** |  | [optional] 

