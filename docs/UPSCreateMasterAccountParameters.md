# Shippo::UPSCreateMasterAccountParameters

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**billing_address_city** | **String** |  | 
**billing_address_country_iso2** | **String** |  | 
**billing_address_state** | **String** |  | 
**billing_address_street1** | **String** |  | 
**billing_address_street2** | **String** | Empty string acceptable for billing_address_street2 | [optional] 
**billing_address_zip** | **String** |  | 
**company** | **String** | Company name. Full name is acceptable in this field if the user has no company name | [optional] 
**email** | **String** |  | [optional] 
**full_name** | **String** |  | [optional] 
**phone** | **String** | Needs to be a valid phone number and cannot be null | [optional] 
**pickup_address_city** | **String** | User&#x27;s pickup address city. | 
**pickup_address_country_iso2** | **String** | User&#x27;s pickup street 1. | 
**pickup_address_same_as_billing_address** | **BOOLEAN** |  | [optional] 
**pickup_address_state** | **String** | User&#x27;s pickup address state. | 
**pickup_address_street1** | **String** | User&#x27;s pickup address street 1. | 
**pickup_address_street2** | **String** | User&#x27;s pickup street 2. | [optional] 
**pickup_address_zip** | **String** | User&#x27;s pickup address zip code. | 
**ups_agreements** | **BOOLEAN** | Whether the user agrees to the UPS terms and conditions or not. Error 400 will be returned if passed in as false | 

