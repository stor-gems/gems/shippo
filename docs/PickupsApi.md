# Shippo::PickupsApi

All URIs are relative to *https://platform-api.goshippo.com/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_pickup**](PickupsApi.md#create_pickup) | **POST** /merchants/{MerchantId}/pickups/ | Create a pickup

# **create_pickup**
> Pickup create_pickup(merchant_id, opts)

Create a pickup

Creates a pickup object.

### Example
```ruby
# load the gem
require 'shippo'
# setup authorization
Shippo.configure do |config|
  # Configure API key authorization: APIKeyHeader
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = Shippo::PickupsApi.new
merchant_id = 'merchant_id_example' # String | description: Object ID of the merchant
opts = { 
  body: Shippo::BasePickup.new # BasePickup | Shippo’s pickups endpoint allows you to schedule pickups with USPS and DHL Express for eligible shipments that you have already created.
  shippo_api_version: 'shippo_api_version_example' # String | description: String used to pick a non-default api version to use
}

begin
  #Create a pickup
  result = api_instance.create_pickup(merchant_id, opts)
  p result
rescue Shippo::ApiError => e
  puts "Exception when calling PickupsApi->create_pickup: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **merchant_id** | **String**| description: Object ID of the merchant | 
 **body** | [**BasePickup**](BasePickup.md)| Shippo’s pickups endpoint allows you to schedule pickups with USPS and DHL Express for eligible shipments that you have already created. | [optional] 
 **shippo_api_version** | **String**| description: String used to pick a non-default api version to use | [optional] 

### Return type

[**Pickup**](Pickup.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



