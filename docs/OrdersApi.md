# Shippo::OrdersApi

All URIs are relative to *https://platform-api.goshippo.com/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_order**](OrdersApi.md#create_order) | **POST** /merchants/{MerchantId}/orders/ | Create a new order
[**list_order**](OrdersApi.md#list_order) | **GET** /merchants/{MerchantId}/orders/ | List all orders
[**get_order**](OrdersApi.md#get_order) | **GET** /merchants/{MerchantId}/orders/{OrderId}/ | Retrieve an order

# **create_order**
> Order create_order(merchant_id, opts)

Create a new order

Creates a new order object.

### Example
```ruby
# load the gem
require 'shippo'
# setup authorization
Shippo.configure do |config|
  # Configure API key authorization: APIKeyHeader
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = Shippo::OrdersApi.new
merchant_id = 'merchant_id_example' # String | description: Object ID of the merchant
opts = { 
  body: Shippo::OrderRequest.new # OrderRequest | Order details.
  shippo_api_version: 'shippo_api_version_example' # String | description: String used to pick a non-default api version to use
}

begin
  #Create a new order
  result = api_instance.create_order(merchant_id, opts)
  p result
rescue Shippo::ApiError => e
  puts "Exception when calling OrdersApi->create_order: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **merchant_id** | **String**| description: Object ID of the merchant | 
 **body** | [**OrderRequest**](OrderRequest.md)| Order details. | [optional] 
 **shippo_api_version** | **String**| description: String used to pick a non-default api version to use | [optional] 

### Return type

[**Order**](Order.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **list_order**
> PaginatedOrderResponse list_order(merchant_id, opts)

List all orders

List all order objects.

### Example
```ruby
# load the gem
require 'shippo'
# setup authorization
Shippo.configure do |config|
  # Configure API key authorization: APIKeyHeader
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = Shippo::OrdersApi.new
merchant_id = 'merchant_id_example' # String | description: Object ID of the merchant
opts = { 
  shippo_api_version: 'shippo_api_version_example', # String | description: String used to pick a non-default api version to use
  page: 1, # Integer | description: The page number you want to select
  results: 25 # Integer | description: The number of results to return per page (max 100)
}

begin
  #List all orders
  result = api_instance.list_order(merchant_id, opts)
  p result
rescue Shippo::ApiError => e
  puts "Exception when calling OrdersApi->list_order: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **merchant_id** | **String**| description: Object ID of the merchant | 
 **shippo_api_version** | **String**| description: String used to pick a non-default api version to use | [optional] 
 **page** | **Integer**| description: The page number you want to select | [optional] [default to 1]
 **results** | **Integer**| description: The number of results to return per page (max 100) | [optional] [default to 25]

### Return type

[**PaginatedOrderResponse**](PaginatedOrderResponse.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **get_order**
> Order get_order(merchant_id, opts)

Retrieve an order

Retrieve an existing order by object id.

### Example
```ruby
# load the gem
require 'shippo'
# setup authorization
Shippo.configure do |config|
  # Configure API key authorization: APIKeyHeader
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = Shippo::OrdersApi.new
merchant_id = 'merchant_id_example' # String | description: Object ID of the merchant
opts = { 
  shippo_api_version: 'shippo_api_version_example' # String | description: String used to pick a non-default api version to use
}

begin
  #Retrieve an order
  result = api_instance.get_order(merchant_id, opts)
  p result
rescue Shippo::ApiError => e
  puts "Exception when calling OrdersApi->get_order: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **merchant_id** | **String**| description: Object ID of the merchant | 
 **shippo_api_version** | **String**| description: String used to pick a non-default api version to use | [optional] 

### Return type

[**Order**](Order.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



