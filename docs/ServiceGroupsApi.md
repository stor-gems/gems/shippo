# Shippo::ServiceGroupsApi

All URIs are relative to *https://platform-api.goshippo.com/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_service_group**](ServiceGroupsApi.md#create_service_group) | **POST** /merchants/{MerchantId}/service-groups/ | Create a new service group
[**delete_service_group**](ServiceGroupsApi.md#delete_service_group) | **DELETE** /merchants/{MerchantId}/service-groups/{ServiceGroupId}/ | Delete a service group
[**list_service_group**](ServiceGroupsApi.md#list_service_group) | **GET** /merchants/{MerchantId}/service-groups/ | List all service groups
[**update_service_group**](ServiceGroupsApi.md#update_service_group) | **PUT** /merchants/{MerchantId}/service-groups/ | Update an existing service group

# **create_service_group**
> ServiceGroupResponse create_service_group(opts)

Create a new service group

Creates a new service group.

### Example
```ruby
# load the gem
require 'shippo'
# setup authorization
Shippo.configure do |config|
  # Configure API key authorization: APIKeyHeader
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = Shippo::ServiceGroupsApi.new
opts = { 
  body: Shippo::BaseServiceGroup.new # BaseServiceGroup | 
}

begin
  #Create a new service group
  result = api_instance.create_service_group(opts)
  p result
rescue Shippo::ApiError => e
  puts "Exception when calling ServiceGroupsApi->create_service_group: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**BaseServiceGroup**](BaseServiceGroup.md)|  | [optional] 

### Return type

[**ServiceGroupResponse**](ServiceGroupResponse.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **delete_service_group**
> delete_service_group

Delete a service group

Delete a service group by object ID.

### Example
```ruby
# load the gem
require 'shippo'
# setup authorization
Shippo.configure do |config|
  # Configure API key authorization: APIKeyHeader
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = Shippo::ServiceGroupsApi.new

begin
  #Delete a service group
  api_instance.delete_service_group
rescue Shippo::ApiError => e
  puts "Exception when calling ServiceGroupsApi->delete_service_group: #{e}"
end
```

### Parameters
This endpoint does not need any parameter.

### Return type

nil (empty response body)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined



# **list_service_group**
> Array&lt;ServiceGroupResponse&gt; list_service_group

List all service groups

List all service group objects.

### Example
```ruby
# load the gem
require 'shippo'
# setup authorization
Shippo.configure do |config|
  # Configure API key authorization: APIKeyHeader
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = Shippo::ServiceGroupsApi.new

begin
  #List all service groups
  result = api_instance.list_service_group
  p result
rescue Shippo::ApiError => e
  puts "Exception when calling ServiceGroupsApi->list_service_group: #{e}"
end
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**Array&lt;ServiceGroupResponse&gt;**](ServiceGroupResponse.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **update_service_group**
> ServiceGroupResponse update_service_group(opts)

Update an existing service group

PUT request to update an existing service group object. The object_id cannot be updated as it is the unique identifier for the object.

### Example
```ruby
# load the gem
require 'shippo'
# setup authorization
Shippo.configure do |config|
  # Configure API key authorization: APIKeyHeader
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = Shippo::ServiceGroupsApi.new
opts = { 
  body: Shippo::ServiceGroup.new # ServiceGroup | 
}

begin
  #Update an existing service group
  result = api_instance.update_service_group(opts)
  p result
rescue Shippo::ApiError => e
  puts "Exception when calling ServiceGroupsApi->update_service_group: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ServiceGroup**](ServiceGroup.md)|  | [optional] 

### Return type

[**ServiceGroupResponse**](ServiceGroupResponse.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



