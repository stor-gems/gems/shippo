# Shippo::CreateDPDUKMasterAccountRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**carrier** | **String** |  | 
**parameters** | **Object** |  | 

