# Shippo::AddressValidationResultsMessage

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **String** |  | [optional] 
**source** | **String** |  | [optional] 
**text** | **String** |  | [optional] 
**type** | **String** |  | [optional] 

