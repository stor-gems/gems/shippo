# Shippo::RefundsApi

All URIs are relative to *https://platform-api.goshippo.com/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_refund**](RefundsApi.md#create_refund) | **POST** /merchants/{MerchantId}/refunds/ | Create a refund
[**get_refund**](RefundsApi.md#get_refund) | **GET** /merchants/{MerchantId}/refunds/{RefundId}/ | Retrieve a refund

# **create_refund**
> Refund create_refund(merchant_id, opts)

Create a refund

Creates a new refund object.

### Example
```ruby
# load the gem
require 'shippo'
# setup authorization
Shippo.configure do |config|
  # Configure API key authorization: APIKeyHeader
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = Shippo::RefundsApi.new
merchant_id = 'merchant_id_example' # String | description: Object ID of the merchant
opts = { 
  body: Shippo::RefundRequestBody.new # RefundRequestBody | Refund details
  shippo_api_version: 'shippo_api_version_example' # String | description: String used to pick a non-default api version to use
}

begin
  #Create a refund
  result = api_instance.create_refund(merchant_id, opts)
  p result
rescue Shippo::ApiError => e
  puts "Exception when calling RefundsApi->create_refund: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **merchant_id** | **String**| description: Object ID of the merchant | 
 **body** | [**RefundRequestBody**](RefundRequestBody.md)| Refund details | [optional] 
 **shippo_api_version** | **String**| description: String used to pick a non-default api version to use | [optional] 

### Return type

[**Refund**](Refund.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **get_refund**
> Refund get_refund(merchant_id, refund_id, opts)

Retrieve a refund

Retrieve an existing refund by object id.

### Example
```ruby
# load the gem
require 'shippo'
# setup authorization
Shippo.configure do |config|
  # Configure API key authorization: APIKeyHeader
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = Shippo::RefundsApi.new
merchant_id = 'merchant_id_example' # String | description: Object ID of the merchant
refund_id = 'refund_id_example' # String | description: Object ID of the refund to update
opts = { 
  shippo_api_version: 'shippo_api_version_example' # String | description: String used to pick a non-default api version to use
}

begin
  #Retrieve a refund
  result = api_instance.get_refund(merchant_id, refund_id, opts)
  p result
rescue Shippo::ApiError => e
  puts "Exception when calling RefundsApi->get_refund: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **merchant_id** | **String**| description: Object ID of the merchant | 
 **refund_id** | **String**| description: Object ID of the refund to update | 
 **shippo_api_version** | **String**| description: String used to pick a non-default api version to use | [optional] 

### Return type

[**Refund**](Refund.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



