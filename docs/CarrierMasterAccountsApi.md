# Shippo::CarrierMasterAccountsApi

All URIs are relative to *https://platform-api.goshippo.com/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**carrier_register_status**](CarrierMasterAccountsApi.md#carrier_register_status) | **GET** /merchants/{MerchantId}/carrier_accounts/reg-status/ | Get Carrier Registration status
[**platform_carrier_registration**](CarrierMasterAccountsApi.md#platform_carrier_registration) | **POST** /merchants/{MerchantId}/carrier_accounts/register/new/ | Add a Shippo master carrier account

# **carrier_register_status**
> CarrierRegisterStatusResponse carrier_register_status(carrier, merchant_id, opts)

Get Carrier Registration status

Gets the registration status for the given account for the given carrier

### Example
```ruby
# load the gem
require 'shippo'
# setup authorization
Shippo.configure do |config|
  # Configure API key authorization: APIKeyHeader
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = Shippo::CarrierMasterAccountsApi.new
carrier = 'carrier_example' # String | 
merchant_id = 'merchant_id_example' # String | description: Object ID of the merchant
opts = { 
  shippo_api_version: 'shippo_api_version_example' # String | description: String used to pick a non-default api version to use
}

begin
  #Get Carrier Registration status
  result = api_instance.carrier_register_status(carrier, merchant_id, opts)
  p result
rescue Shippo::ApiError => e
  puts "Exception when calling CarrierMasterAccountsApi->carrier_register_status: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **carrier** | **String**|  | 
 **merchant_id** | **String**| description: Object ID of the merchant | 
 **shippo_api_version** | **String**| description: String used to pick a non-default api version to use | [optional] 

### Return type

[**CarrierRegisterStatusResponse**](CarrierRegisterStatusResponse.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **platform_carrier_registration**
> PlatformCarrierRegisterResponse platform_carrier_registration(opts)

Add a Shippo master carrier account

Add a Shippo master carrier account to the merchant

### Example
```ruby
# load the gem
require 'shippo'
# setup authorization
Shippo.configure do |config|
  # Configure API key authorization: APIKeyHeader
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = Shippo::CarrierMasterAccountsApi.new
opts = { 
  body: Shippo::CreateCanadaPostMasterAccountRequest.new # CreateCanadaPostMasterAccountRequest | Examples.
}

begin
  #Add a Shippo master carrier account
  result = api_instance.platform_carrier_registration(opts)
  p result
rescue Shippo::ApiError => e
  puts "Exception when calling CarrierMasterAccountsApi->platform_carrier_registration: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**CreateCanadaPostMasterAccountRequest**](CreateCanadaPostMasterAccountRequest.md)| Examples. | [optional] 

### Return type

[**PlatformCarrierRegisterResponse**](PlatformCarrierRegisterResponse.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



