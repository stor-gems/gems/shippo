# Shippo::CustomsItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**description** | **String** |  | 
**eccn_ear99** | **String** |  | [optional] 
**mass_unit** | **String** |  | 
**metadata** | **String** |  | [optional] 
**net_weight** | **String** |  | 
**object_created** | **DateTime** |  | [optional] 
**object_id** | **String** |  | [optional] 
**object_owner** | **String** |  | [optional] 
**object_state** | **String** |  | [optional] 
**object_updated** | **DateTime** |  | [optional] 
**origin_country** | **String** |  | 
**quantity** | **Integer** |  | 
**sku_code** | **String** | The stock keeping unit value of this item. | [optional] 
**tariff_number** | **String** |  | [optional] 
**test** | **BOOLEAN** |  | [optional] 
**value_amount** | **String** |  | 
**value_currency** | **String** |  | 

