# Shippo::CarrierParcelTemplateStruct

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**carrier** | **String** | The name of the carrier that provides this parcel template | [optional] 
**distance_unit** | **String** | The measure unit of the package dimensions in inches, cm, or mm. | [optional] 
**height** | **String** | The height of the package, in units specified by the distance_unit attribute | [optional] 
**is_variable_dimensions** | **String** | True if the carrier parcel template allows custom dimensions, such as USPS Softpack. | [optional] 
**length** | **String** | The length of the package, in units specified by the distance_unit attribute | [optional] 
**name** | **String** | The name of the carrier parcel template | [optional] 
**token** | **String** | The unique string representation of the carrier parcel template | [optional] 
**width** | **String** | The width of the package, in units specified by the distance_unit attribute | [optional] 

