# Shippo::BaseServiceGroup

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**description** | **String** |  | 
**flat_rate** | **String** | String representation of an amount to be returned as the flat rate if 1. The service group is of type LIVE_RATE and no matching rates were found; or 2. The service group is of type FLAT_RATE. Either integers or decimals are accepted. Required unless type is FREE_SHIPPING | [optional] 
**flat_rate_currency** | **String** | required unless type is FREE_SHIPPING | [optional] 
**free_shipping_threshold_currency** | **String** | optional unless type is FREE_SHIPPING | [optional] 
**free_shipping_threshold_min** | **String** | For service groups of type FREE_SHIPPING, this field must be required to configure the minimum cart total (total cost of items in the cart) for this service group to be returned for rates at checkout. Optional unless type is FREE_SHIPPING | [optional] 
**name** | **String** |  | 
**rate_adjustment** | **Integer** |  | [optional] 
**service_levels** | [**Array&lt;ServiceLevel&gt;**](ServiceLevel.md) |  | 
**type** | **String** |  | 

