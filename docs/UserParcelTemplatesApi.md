# Shippo::UserParcelTemplatesApi

All URIs are relative to *https://platform-api.goshippo.com/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_user_parcel_template**](UserParcelTemplatesApi.md#create_user_parcel_template) | **POST** /merchants/{MerchantId}/user-parcel-templates/ | Create a new user parcel template
[**delete_user_parcel_template**](UserParcelTemplatesApi.md#delete_user_parcel_template) | **DELETE** /merchants/{MerchantId}/user-parcel-templates/{UserParcelTemplateObjectId}/ | Delete a user parcel template
[**list_user_parcel_templates**](UserParcelTemplatesApi.md#list_user_parcel_templates) | **GET** /merchants/{MerchantId}/user-parcel-templates/ | List all user parcel templates
[**get_user_parcel_template**](UserParcelTemplatesApi.md#get_user_parcel_template) | **GET** /merchants/{MerchantId}/user-parcel-templates/{UserParcelTemplateObjectId}/ | Retrieves an user parcel templates
[**update_user_parcel_template**](UserParcelTemplatesApi.md#update_user_parcel_template) | **PUT** /merchants/{MerchantId}/user-parcel-templates/{UserParcelTemplateObjectId} | Update an existing user parcel template

# **create_user_parcel_template**
> UserParcelTemplateStruct create_user_parcel_template(opts)

Create a new user parcel template

Creates a new user parcel template. You can choose to create a parcel template using a preset carrier template as a starting point, or you can create an entirely custom one. To use a preset carrier template, pass in a unique template token from [this list](https://goshippo.com/docs/reference#parcel-templates), plus the weight fields (**weight** and **weight_unit**). Otherwise, omit the template field and pass the other fields, for the weight, length, height, and depth, as well as their units.

### Example
```ruby
# load the gem
require 'shippo'
# setup authorization
Shippo.configure do |config|
  # Configure API key authorization: APIKeyHeader
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = Shippo::UserParcelTemplatesApi.new
opts = { 
  body: Shippo::UserParcelTemplateRequestBodyWrapper.new # UserParcelTemplateRequestBodyWrapper | 
}

begin
  #Create a new user parcel template
  result = api_instance.create_user_parcel_template(opts)
  p result
rescue Shippo::ApiError => e
  puts "Exception when calling UserParcelTemplatesApi->create_user_parcel_template: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**UserParcelTemplateRequestBodyWrapper**](UserParcelTemplateRequestBodyWrapper.md)|  | [optional] 

### Return type

[**UserParcelTemplateStruct**](UserParcelTemplateStruct.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **delete_user_parcel_template**
> delete_user_parcel_template(user_parcel_template_token)

Delete a user parcel template

Delete a user parcel template by object ID.

### Example
```ruby
# load the gem
require 'shippo'
# setup authorization
Shippo.configure do |config|
  # Configure API key authorization: APIKeyHeader
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = Shippo::UserParcelTemplatesApi.new
user_parcel_template_token = 'user_parcel_template_token_example' # String | The unique string representation of the user parcel template


begin
  #Delete a user parcel template
  api_instance.delete_user_parcel_template(user_parcel_template_token)
rescue Shippo::ApiError => e
  puts "Exception when calling UserParcelTemplatesApi->delete_user_parcel_template: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_parcel_template_token** | **String**| The unique string representation of the user parcel template | 

### Return type

nil (empty response body)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined



# **list_user_parcel_templates**
> AllUserParcelTemplateResponse list_user_parcel_templates

List all user parcel templates

List all user parcel template objects.

### Example
```ruby
# load the gem
require 'shippo'
# setup authorization
Shippo.configure do |config|
  # Configure API key authorization: APIKeyHeader
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = Shippo::UserParcelTemplatesApi.new

begin
  #List all user parcel templates
  result = api_instance.list_user_parcel_templates
  p result
rescue Shippo::ApiError => e
  puts "Exception when calling UserParcelTemplatesApi->list_user_parcel_templates: #{e}"
end
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**AllUserParcelTemplateResponse**](AllUserParcelTemplateResponse.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **get_user_parcel_template**
> UserParcelTemplateStruct get_user_parcel_template(user_parcel_template_token)

Retrieves an user parcel templates

Fetches the parcel template information for a specific user parcel template, identified by the object ID.

### Example
```ruby
# load the gem
require 'shippo'
# setup authorization
Shippo.configure do |config|
  # Configure API key authorization: APIKeyHeader
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = Shippo::UserParcelTemplatesApi.new
user_parcel_template_token = 'user_parcel_template_token_example' # String | The unique string representation of the user parcel template


begin
  #Retrieves an user parcel templates
  result = api_instance.get_user_parcel_template(user_parcel_template_token)
  p result
rescue Shippo::ApiError => e
  puts "Exception when calling UserParcelTemplatesApi->get_user_parcel_template: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_parcel_template_token** | **String**| The unique string representation of the user parcel template | 

### Return type

[**UserParcelTemplateStruct**](UserParcelTemplateStruct.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **update_user_parcel_template**
> UserParcelTemplateStruct update_user_parcel_template(user_parcel_template_token, opts)

Update an existing user parcel template

Updates an existing user parcel template.

### Example
```ruby
# load the gem
require 'shippo'
# setup authorization
Shippo.configure do |config|
  # Configure API key authorization: APIKeyHeader
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = Shippo::UserParcelTemplatesApi.new
user_parcel_template_token = 'user_parcel_template_token_example' # String | The unique string representation of the user parcel template
opts = { 
  body: Shippo::UserParcelTemplateRequestBodyWrapper.new # UserParcelTemplateRequestBodyWrapper | Address details.
}

begin
  #Update an existing user parcel template
  result = api_instance.update_user_parcel_template(user_parcel_template_token, opts)
  p result
rescue Shippo::ApiError => e
  puts "Exception when calling UserParcelTemplatesApi->update_user_parcel_template: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_parcel_template_token** | **String**| The unique string representation of the user parcel template | 
 **body** | [**UserParcelTemplateRequestBodyWrapper**](UserParcelTemplateRequestBodyWrapper.md)| Address details. | [optional] 

### Return type

[**UserParcelTemplateStruct**](UserParcelTemplateStruct.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



