# Shippo::TrackingStatusApi

All URIs are relative to *https://platform-api.goshippo.com/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_track_post**](TrackingStatusApi.md#create_track_post) | **POST** /merchants/{MerchantId}/tracks/ | Register a tracking webhook
[**get_track**](TrackingStatusApi.md#get_track) | **GET** /merchants/{MerchantId}/tracks/{Carrier}/{TrackingNumber} | Get a tracking status

# **create_track_post**
> Track create_track_post(merchant_id, opts)

Register a tracking webhook

You can register your webhook(s) for a Shipment (and request the current status at the same time) by POSTing to the tracking endpoint. This way Shippo will send HTTP notifications to your track_updated webhook(s) whenever the status changes.

### Example
```ruby
# load the gem
require 'shippo'
# setup authorization
Shippo.configure do |config|
  # Configure API key authorization: APIKeyHeader
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = Shippo::TrackingStatusApi.new
merchant_id = 'merchant_id_example' # String | description: Object ID of the merchant
opts = { 
  body: Shippo::TracksRequest.new # TracksRequest | 
  shippo_api_version: 'shippo_api_version_example' # String | description: String used to pick a non-default api version to use
}

begin
  #Register a tracking webhook
  result = api_instance.create_track_post(merchant_id, opts)
  p result
rescue Shippo::ApiError => e
  puts "Exception when calling TrackingStatusApi->create_track_post: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **merchant_id** | **String**| description: Object ID of the merchant | 
 **body** | [**TracksRequest**](TracksRequest.md)|  | [optional] 
 **shippo_api_version** | **String**| description: String used to pick a non-default api version to use | [optional] 

### Return type

[**Track**](Track.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **get_track**
> Track get_track(merchant_id, opts)

Get a tracking status

Request the tracking status of a shipment by sending a GET request.

### Example
```ruby
# load the gem
require 'shippo'
# setup authorization
Shippo.configure do |config|
  # Configure API key authorization: APIKeyHeader
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = Shippo::TrackingStatusApi.new
merchant_id = 'merchant_id_example' # String | description: Object ID of the merchant
opts = { 
  shippo_api_version: 'shippo_api_version_example' # String | description: String used to pick a non-default api version to use
}

begin
  #Get a tracking status
  result = api_instance.get_track(merchant_id, opts)
  p result
rescue Shippo::ApiError => e
  puts "Exception when calling TrackingStatusApi->get_track: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **merchant_id** | **String**| description: Object ID of the merchant | 
 **shippo_api_version** | **String**| description: String used to pick a non-default api version to use | [optional] 

### Return type

[**Track**](Track.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



