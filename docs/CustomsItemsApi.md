# Shippo::CustomsItemsApi

All URIs are relative to *https://platform-api.goshippo.com/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_customs_item**](CustomsItemsApi.md#create_customs_item) | **POST** /merchants/{MerchantId}/customs/items/ | Create a new customs item
[**list_customs_items**](CustomsItemsApi.md#list_customs_items) | **GET** /merchants/{MerchantId}/customs/items/ | List all customs items
[**get_customs_item**](CustomsItemsApi.md#get_customs_item) | **GET** /merchants/{MerchantId}/customs/items/{CustomsItemId}/ | Retrieve a customs item

# **create_customs_item**
> CustomsItem create_customs_item(merchant_id, opts)

Create a new customs item

Creates a new customs item object

### Example
```ruby
# load the gem
require 'shippo'
# setup authorization
Shippo.configure do |config|
  # Configure API key authorization: APIKeyHeader
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = Shippo::CustomsItemsApi.new
merchant_id = 'merchant_id_example' # String | description: Object ID of the merchant
opts = { 
  body: Shippo::Item.new # Item | CustomsItem details.
  shippo_api_version: 'shippo_api_version_example' # String | description: String used to pick a non-default api version to use
}

begin
  #Create a new customs item
  result = api_instance.create_customs_item(merchant_id, opts)
  p result
rescue Shippo::ApiError => e
  puts "Exception when calling CustomsItemsApi->create_customs_item: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **merchant_id** | **String**| description: Object ID of the merchant | 
 **body** | [**Item**](Item.md)| CustomsItem details. | [optional] 
 **shippo_api_version** | **String**| description: String used to pick a non-default api version to use | [optional] 

### Return type

[**CustomsItem**](CustomsItem.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **list_customs_items**
> PaginatedCustomsItemsResponse list_customs_items(merchant_id, opts)

List all customs items

List all customs items objects

### Example
```ruby
# load the gem
require 'shippo'
# setup authorization
Shippo.configure do |config|
  # Configure API key authorization: APIKeyHeader
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = Shippo::CustomsItemsApi.new
merchant_id = 'merchant_id_example' # String | description: Object ID of the merchant
opts = { 
  shippo_api_version: 'shippo_api_version_example', # String | description: String used to pick a non-default api version to use
  page: 1, # Integer | description: The page number you want to select
  results: 25 # Integer | description: The number of results to return per page (max 100)
}

begin
  #List all customs items
  result = api_instance.list_customs_items(merchant_id, opts)
  p result
rescue Shippo::ApiError => e
  puts "Exception when calling CustomsItemsApi->list_customs_items: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **merchant_id** | **String**| description: Object ID of the merchant | 
 **shippo_api_version** | **String**| description: String used to pick a non-default api version to use | [optional] 
 **page** | **Integer**| description: The page number you want to select | [optional] [default to 1]
 **results** | **Integer**| description: The number of results to return per page (max 100) | [optional] [default to 25]

### Return type

[**PaginatedCustomsItemsResponse**](PaginatedCustomsItemsResponse.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **get_customs_item**
> CustomsItem get_customs_item(customs_item_id, merchant_id, opts)

Retrieve a customs item

Retrieve an existing customs item by object id

### Example
```ruby
# load the gem
require 'shippo'
# setup authorization
Shippo.configure do |config|
  # Configure API key authorization: APIKeyHeader
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = Shippo::CustomsItemsApi.new
customs_item_id = 'customs_item_id_example' # String | description: Object ID of the customs item
merchant_id = 'merchant_id_example' # String | description: Object ID of the merchant
opts = { 
  shippo_api_version: 'shippo_api_version_example', # String | description: String used to pick a non-default api version to use
  page: 1 # Integer | description: The page number you want to select
}

begin
  #Retrieve a customs item
  result = api_instance.get_customs_item(customs_item_id, merchant_id, opts)
  p result
rescue Shippo::ApiError => e
  puts "Exception when calling CustomsItemsApi->get_customs_item: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customs_item_id** | **String**| description: Object ID of the customs item | 
 **merchant_id** | **String**| description: Object ID of the merchant | 
 **shippo_api_version** | **String**| description: String used to pick a non-default api version to use | [optional] 
 **page** | **Integer**| description: The page number you want to select | [optional] [default to 1]

### Return type

[**CustomsItem**](CustomsItem.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



