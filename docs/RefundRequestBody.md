# Shippo::RefundRequestBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**async** | **BOOLEAN** |  | [optional] 
**transaction** | **String** |  | 

