# Shippo::PaginatedCustomsItemsResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_next** | **String** |  | [optional] 
**previous** | **String** |  | [optional] 
**results** | [**Array&lt;CustomsItem&gt;**](CustomsItem.md) |  | [optional] 

