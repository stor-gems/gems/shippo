# Shippo::CarrierParcelTemplatesApi

All URIs are relative to *https://platform-api.goshippo.com/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**list_carrier_parcel_templates**](CarrierParcelTemplatesApi.md#list_carrier_parcel_templates) | **GET** /parcel-templates/ | List all carrier parcel templates
[**get_carrier_parcel_template**](CarrierParcelTemplatesApi.md#get_carrier_parcel_template) | **GET** /parcel-templates/{CarrierParcelTemplateToken}/ | Retrieve a carrier parcel templates

# **list_carrier_parcel_templates**
> Array&lt;CarrierParcelTemplateStruct&gt; list_carrier_parcel_templates(opts)

List all carrier parcel templates

List all carrier parcel template objects.

### Example
```ruby
# load the gem
require 'shippo'
# setup authorization
Shippo.configure do |config|
  # Configure API key authorization: APIKeyHeader
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = Shippo::CarrierParcelTemplatesApi.new
opts = { 
  include: 'include_example', # String | 
  carrier: 'carrier_example' # String | filter by specific carrier
}

begin
  #List all carrier parcel templates
  result = api_instance.list_carrier_parcel_templates(opts)
  p result
rescue Shippo::ApiError => e
  puts "Exception when calling CarrierParcelTemplatesApi->list_carrier_parcel_templates: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **include** | **String**|  | [optional] 
 **carrier** | **String**| filter by specific carrier | [optional] 

### Return type

[**Array&lt;CarrierParcelTemplateStruct&gt;**](CarrierParcelTemplateStruct.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **get_carrier_parcel_template**
> CarrierParcelTemplateStruct get_carrier_parcel_template(carrier_parcel_template_token)

Retrieve a carrier parcel templates

Fetches the parcel template information for a specific carrier parcel template, identified by the token.

### Example
```ruby
# load the gem
require 'shippo'
# setup authorization
Shippo.configure do |config|
  # Configure API key authorization: APIKeyHeader
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = Shippo::CarrierParcelTemplatesApi.new
carrier_parcel_template_token = 'carrier_parcel_template_token_example' # String | The unique string representation of the carrier parcel template


begin
  #Retrieve a carrier parcel templates
  result = api_instance.get_carrier_parcel_template(carrier_parcel_template_token)
  p result
rescue Shippo::ApiError => e
  puts "Exception when calling CarrierParcelTemplatesApi->get_carrier_parcel_template: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **carrier_parcel_template_token** | **String**| The unique string representation of the carrier parcel template | 

### Return type

[**CarrierParcelTemplateStruct**](CarrierParcelTemplateStruct.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



