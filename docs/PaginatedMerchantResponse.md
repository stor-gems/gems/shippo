# Shippo::PaginatedMerchantResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_next** | **String** |  | [optional] 
**previous** | **String** |  | [optional] 
**results** | [**Array&lt;Merchant&gt;**](Merchant.md) |  | [optional] 

