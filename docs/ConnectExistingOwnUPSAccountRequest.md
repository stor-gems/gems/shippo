# Shippo::ConnectExistingOwnUPSAccountRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_id** | **String** | This field is required as input to the API, but is not used for UPS. Use the empty string. | 
**active** | **BOOLEAN** |  | 
**carrier** | **String** |  | [optional] 
**metadata** | **String** |  | [optional] 
**parameters** | [**UPSConnectExistingOwnAccountParameters**](UPSConnectExistingOwnAccountParameters.md) |  | 
**test** | **BOOLEAN** |  | [optional] 

