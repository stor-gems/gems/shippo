# Shippo::GetLiveRateResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**result** | [**LiveRateParcelTemplate**](LiveRateParcelTemplate.md) |  | [optional] 

