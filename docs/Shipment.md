# Shippo::Shipment

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address_from** | [**Address**](Address.md) |  | 
**address_return** | [**Address**](Address.md) |  | [optional] 
**address_to** | [**Address**](Address.md) |  | 
**async** | **BOOLEAN** |  | [optional] 
**carrier_accounts** | **Array&lt;Object&gt;** |  | 
**customs_declaration** | **String** |  | [optional] 
**extra** | [**Extra**](Extra.md) |  | [optional] 
**messages** | [**Array&lt;ShipmentMessages&gt;**](ShipmentMessages.md) |  | 
**metadata** | **String** |  | 
**object_created** | **DateTime** |  | 
**object_id** | **String** |  | 
**object_owner** | **String** |  | 
**object_updated** | **DateTime** |  | 
**parcels** | [**Array&lt;ParcelTemplate&gt;**](ParcelTemplate.md) |  | 
**rates** | [**Array&lt;Rate&gt;**](Rate.md) |  | 
**shipment_date** | **String** |  | [optional] 
**status** | **String** |  | 
**test** | **BOOLEAN** |  | [optional] 

