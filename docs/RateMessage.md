# Shippo::RateMessage

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **String** |  | [optional] 
**source** | **String** |  | [optional] 
**text** | **String** |  | [optional] 

