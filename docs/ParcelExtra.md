# Shippo::ParcelExtra

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cod** | [**COD**](COD.md) |  | [optional] 
**insurance** | [**ParcelInsurance**](ParcelInsurance.md) |  | [optional] 

