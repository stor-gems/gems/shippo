# Shippo::AddressValidationResults

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**is_valid** | **BOOLEAN** |  | [optional] 
**messages** | [**Array&lt;AddressValidationResultsMessage&gt;**](AddressValidationResultsMessage.md) |  | [optional] 

