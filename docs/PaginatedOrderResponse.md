# Shippo::PaginatedOrderResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_next** | **String** |  | [optional] 
**previous** | **String** |  | [optional] 
**results** | [**Array&lt;Order&gt;**](Order.md) |  | [optional] 

