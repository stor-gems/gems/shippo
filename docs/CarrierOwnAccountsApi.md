# Shippo::CarrierOwnAccountsApi

All URIs are relative to *https://platform-api.goshippo.com/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**platform_carrier_own_account_creation**](CarrierOwnAccountsApi.md#platform_carrier_own_account_creation) | **POST** /merchants/{MerchantId}/carrier_accounts/ | Create a new carrier account
[**platform_carrier_own_account_list**](CarrierOwnAccountsApi.md#platform_carrier_own_account_list) | **GET** /merchants/{MerchantId}/carrier_accounts/ | List all carrier accounts
[**platform_carrier_own_account_get**](CarrierOwnAccountsApi.md#platform_carrier_own_account_get) | **GET** /merchants/{MerchantId}/carrier_accounts/{CarrierAccountId} | Retrieve a carrier account
[**platform_carrier_own_account_update**](CarrierOwnAccountsApi.md#platform_carrier_own_account_update) | **PUT** /merchants/{MerchantId}/carrier_accounts/{CarrierAccountId} | Update a carrier account

# **platform_carrier_own_account_creation**
> PlatformCarrierOwnAccountCreationResponse platform_carrier_own_account_creation(opts)

Create a new carrier account

Creates a new carrier account or connects an existing carrier account to the Shippo account.

### Example
```ruby
# load the gem
require 'shippo'
# setup authorization
Shippo.configure do |config|
  # Configure API key authorization: APIKeyHeader
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = Shippo::CarrierOwnAccountsApi.new
opts = { 
  body: Shippo::ConnectExistingOwnUPSAccountRequest.new # ConnectExistingOwnUPSAccountRequest | Examples.
}

begin
  #Create a new carrier account
  result = api_instance.platform_carrier_own_account_creation(opts)
  p result
rescue Shippo::ApiError => e
  puts "Exception when calling CarrierOwnAccountsApi->platform_carrier_own_account_creation: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ConnectExistingOwnUPSAccountRequest**](ConnectExistingOwnUPSAccountRequest.md)| Examples. | [optional] 

### Return type

[**PlatformCarrierOwnAccountCreationResponse**](PlatformCarrierOwnAccountCreationResponse.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **platform_carrier_own_account_list**
> PlatformCarrierOwnAccountListResponse platform_carrier_own_account_list(merchant_id, opts)

List all carrier accounts

List all carrier accounts.

### Example
```ruby
# load the gem
require 'shippo'
# setup authorization
Shippo.configure do |config|
  # Configure API key authorization: APIKeyHeader
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = Shippo::CarrierOwnAccountsApi.new
merchant_id = 'merchant_id_example' # String | description: Object ID of the merchant
opts = { 
  shippo_api_version: 'shippo_api_version_example', # String | description: String used to pick a non-default api version to use
  page: 1, # Integer | description: The page number you want to select
  results: 25 # Integer | description: The number of results to return per page (max 100)
}

begin
  #List all carrier accounts
  result = api_instance.platform_carrier_own_account_list(merchant_id, opts)
  p result
rescue Shippo::ApiError => e
  puts "Exception when calling CarrierOwnAccountsApi->platform_carrier_own_account_list: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **merchant_id** | **String**| description: Object ID of the merchant | 
 **shippo_api_version** | **String**| description: String used to pick a non-default api version to use | [optional] 
 **page** | **Integer**| description: The page number you want to select | [optional] [default to 1]
 **results** | **Integer**| description: The number of results to return per page (max 100) | [optional] [default to 25]

### Return type

[**PlatformCarrierOwnAccountListResponse**](PlatformCarrierOwnAccountListResponse.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **platform_carrier_own_account_get**
> PlatformCarrierOwnAccountCreationResponse platform_carrier_own_account_get(carrier_account_id, merchant_id, opts)

Retrieve a carrier account

Retrieve an existing carrier account by object id.

### Example
```ruby
# load the gem
require 'shippo'
# setup authorization
Shippo.configure do |config|
  # Configure API key authorization: APIKeyHeader
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = Shippo::CarrierOwnAccountsApi.new
carrier_account_id = 'carrier_account_id_example' # String | description: Object ID of the carrier account
merchant_id = 'merchant_id_example' # String | description: Object ID of the merchant
opts = { 
  shippo_api_version: 'shippo_api_version_example' # String | description: String used to pick a non-default api version to use
}

begin
  #Retrieve a carrier account
  result = api_instance.platform_carrier_own_account_get(carrier_account_id, merchant_id, opts)
  p result
rescue Shippo::ApiError => e
  puts "Exception when calling CarrierOwnAccountsApi->platform_carrier_own_account_get: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **carrier_account_id** | **String**| description: Object ID of the carrier account | 
 **merchant_id** | **String**| description: Object ID of the merchant | 
 **shippo_api_version** | **String**| description: String used to pick a non-default api version to use | [optional] 

### Return type

[**PlatformCarrierOwnAccountCreationResponse**](PlatformCarrierOwnAccountCreationResponse.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **platform_carrier_own_account_update**
> PlatformCarrierOwnAccountCreationResponse platform_carrier_own_account_update(opts)

Update a carrier account

PUT request to update an existing carrier account object. The account_id and carrier can't be updated, because they form the unique identifier together.

### Example
```ruby
# load the gem
require 'shippo'
# setup authorization
Shippo.configure do |config|
  # Configure API key authorization: APIKeyHeader
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = Shippo::CarrierOwnAccountsApi.new
opts = { 
  body: Shippo::UpdateCarrierAccountRequest.new # UpdateCarrierAccountRequest | Examples.
}

begin
  #Update a carrier account
  result = api_instance.platform_carrier_own_account_update(opts)
  p result
rescue Shippo::ApiError => e
  puts "Exception when calling CarrierOwnAccountsApi->platform_carrier_own_account_update: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**UpdateCarrierAccountRequest**](UpdateCarrierAccountRequest.md)| Examples. | [optional] 

### Return type

[**PlatformCarrierOwnAccountCreationResponse**](PlatformCarrierOwnAccountCreationResponse.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



