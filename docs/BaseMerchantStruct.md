# Shippo::BaseMerchantStruct

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | **String** |  | 
**first_name** | **String** |  | 
**last_name** | **String** |  | 
**merchant_name** | **String** |  | 

