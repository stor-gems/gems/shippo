# Shippo::PlatformCarrierOwnAccountListResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_next** | **String** |  | [optional] 
**previous** | **String** |  | [optional] 
**results** | [**Array&lt;PlatformCarrierOwnAccountCreationResponse&gt;**](PlatformCarrierOwnAccountCreationResponse.md) |  | [optional] 

