# Shippo::BatchBatchShipments

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_next** | **String** |  | 
**previous** | **String** |  | 
**results** | [**Array&lt;BatchShipment&gt;**](BatchShipment.md) |  | 

