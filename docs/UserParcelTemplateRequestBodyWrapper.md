# Shippo::UserParcelTemplateRequestBodyWrapper

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**distance_unit** | **String** | The measure unit of the package dimensions in inches, cm, or mm. Required, but if using a preset carrier template then this field must be empty | 
**height** | **String** | The height of the package, in units specified by the distance_unit attribute. Required, but if using a preset carrier template then this field must be empty | 
**length** | **String** | The length of the package, in units specified by the distance_unit attribute. Required, but if using a preset carrier template then this field must be empty | 
**name** | **String** | The name of the User Parcel Template | 
**template** | **String** | The object representing the carrier parcel template, if the template was created from a preset carrier template. Otherwise null. | [optional] 
**weight** | **String** | The weight of the package, in units specified by the weight_unit attribute. | [optional] 
**weight_unit** | **String** | The weight unit of the package, as lb, g, kg, or oz. | [optional] 
**width** | **String** | The width of the package, in units specified by the distance_unit attribute. Required, but if using a preset carrier template then this field must be empty | 

