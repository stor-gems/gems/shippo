# Shippo::ShipmentsApi

All URIs are relative to *https://platform-api.goshippo.com/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_shipment**](ShipmentsApi.md#create_shipment) | **POST** /merchants/{MerchantId}/shipments/ | Create a new shipment
[**list_shipment**](ShipmentsApi.md#list_shipment) | **GET** /merchants/{MerchantId}/shipments/ | List all shipments
[**get_shipment**](ShipmentsApi.md#get_shipment) | **GET** /merchants/{MerchantId}/shipments/{ShipmentId}/ | Retrieve a shipment

# **create_shipment**
> Shipment create_shipment(merchant_id, opts)

Create a new shipment

Creates a new shipment object for the merchant

### Example
```ruby
# load the gem
require 'shippo'
# setup authorization
Shippo.configure do |config|
  # Configure API key authorization: APIKeyHeader
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = Shippo::ShipmentsApi.new
merchant_id = 'merchant_id_example' # String | description: Object ID of the merchant
opts = { 
  body: Shippo::ShipmentInsertRequestBody.new # ShipmentInsertRequestBody | Shipment details and contact info.
  shippo_api_version: 'shippo_api_version_example' # String | description: String used to pick a non-default api version to use
}

begin
  #Create a new shipment
  result = api_instance.create_shipment(merchant_id, opts)
  p result
rescue Shippo::ApiError => e
  puts "Exception when calling ShipmentsApi->create_shipment: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **merchant_id** | **String**| description: Object ID of the merchant | 
 **body** | [**ShipmentInsertRequestBody**](ShipmentInsertRequestBody.md)| Shipment details and contact info. | [optional] 
 **shippo_api_version** | **String**| description: String used to pick a non-default api version to use | [optional] 

### Return type

[**Shipment**](Shipment.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **list_shipment**
> PaginatedShipmentResponse list_shipment(merchant_id, opts)

List all shipments

List all shipment objects for the merchant

### Example
```ruby
# load the gem
require 'shippo'
# setup authorization
Shippo.configure do |config|
  # Configure API key authorization: APIKeyHeader
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = Shippo::ShipmentsApi.new
merchant_id = 'merchant_id_example' # String | description: Object ID of the merchant
opts = { 
  shippo_api_version: 'shippo_api_version_example', # String | description: String used to pick a non-default api version to use
  page: 1, # Integer | description: The page number you want to select
  results: 25 # Integer | description: The number of results to return per page (max 100)
}

begin
  #List all shipments
  result = api_instance.list_shipment(merchant_id, opts)
  p result
rescue Shippo::ApiError => e
  puts "Exception when calling ShipmentsApi->list_shipment: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **merchant_id** | **String**| description: Object ID of the merchant | 
 **shippo_api_version** | **String**| description: String used to pick a non-default api version to use | [optional] 
 **page** | **Integer**| description: The page number you want to select | [optional] [default to 1]
 **results** | **Integer**| description: The number of results to return per page (max 100) | [optional] [default to 25]

### Return type

[**PaginatedShipmentResponse**](PaginatedShipmentResponse.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **get_shipment**
> Shipment get_shipment(merchant_id, shipment_id, opts)

Retrieve a shipment

Retrieve an existing shipment by object id

### Example
```ruby
# load the gem
require 'shippo'
# setup authorization
Shippo.configure do |config|
  # Configure API key authorization: APIKeyHeader
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = Shippo::ShipmentsApi.new
merchant_id = 'merchant_id_example' # String | description: Object ID of the merchant
shipment_id = 'shipment_id_example' # String | description: Object ID of the shipment to update
opts = { 
  shippo_api_version: 'shippo_api_version_example' # String | description: String used to pick a non-default api version to use
}

begin
  #Retrieve a shipment
  result = api_instance.get_shipment(merchant_id, shipment_id, opts)
  p result
rescue Shippo::ApiError => e
  puts "Exception when calling ShipmentsApi->get_shipment: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **merchant_id** | **String**| description: Object ID of the merchant | 
 **shipment_id** | **String**| description: Object ID of the shipment to update | 
 **shippo_api_version** | **String**| description: String used to pick a non-default api version to use | [optional] 

### Return type

[**Shipment**](Shipment.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



