# Shippo::BatchesApi

All URIs are relative to *https://platform-api.goshippo.com/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**add_shipments_to_batch**](BatchesApi.md#add_shipments_to_batch) | **POST** /merchants/{MerchantId}/batches/{BatchId}/add_shipments/ | Add shipments to a batch
[**create_batch**](BatchesApi.md#create_batch) | **POST** /merchants/{MerchantId}/batches/ | Create a batch
[**get_batch**](BatchesApi.md#get_batch) | **GET** /merchants/{MerchantId}/batches/{BatchId}/ | Retrieve a batch
[**purchase_batch**](BatchesApi.md#purchase_batch) | **POST** /merchants/{MerchantId}/batches/{BatchId}/purchase/ | Purchase a batch
[**remove_shipments_from_batch**](BatchesApi.md#remove_shipments_from_batch) | **POST** /merchants/{MerchantId}/batches/{BatchId}/remove_shipments/ | Remove shipments from a batch

# **add_shipments_to_batch**
> Batch add_shipments_to_batch(batch_idmerchant_id, opts)

Add shipments to a batch

Add batch shipments to an existing batch.

### Example
```ruby
# load the gem
require 'shippo'
# setup authorization
Shippo.configure do |config|
  # Configure API key authorization: APIKeyHeader
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = Shippo::BatchesApi.new
batch_id = 'batch_id_example' # String | description: Object ID of the batch
merchant_id = 'merchant_id_example' # String | description: Object ID of the merchant
opts = { 
  body: [Shippo::BaseBatchShipment.new] # Array<BaseBatchShipment> | Array of shipments to add to the batch
  shippo_api_version: 'shippo_api_version_example' # String | description: String used to pick a non-default api version to use
}

begin
  #Add shipments to a batch
  result = api_instance.add_shipments_to_batch(batch_idmerchant_id, opts)
  p result
rescue Shippo::ApiError => e
  puts "Exception when calling BatchesApi->add_shipments_to_batch: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **batch_id** | **String**| description: Object ID of the batch | 
 **merchant_id** | **String**| description: Object ID of the merchant | 
 **body** | [**Array&lt;BaseBatchShipment&gt;**](BaseBatchShipment.md)| Array of shipments to add to the batch | [optional] 
 **shippo_api_version** | **String**| description: String used to pick a non-default api version to use | [optional] 

### Return type

[**Batch**](Batch.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **create_batch**
> Batch create_batch(merchant_id, opts)

Create a batch

Creates a new batch object.

### Example
```ruby
# load the gem
require 'shippo'
# setup authorization
Shippo.configure do |config|
  # Configure API key authorization: APIKeyHeader
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = Shippo::BatchesApi.new
merchant_id = 'merchant_id_example' # String | description: Object ID of the merchant
opts = { 
  body: Shippo::BatchToCreate.new # BatchToCreate | Batch details.
  shippo_api_version: 'shippo_api_version_example' # String | description: String used to pick a non-default api version to use
}

begin
  #Create a batch
  result = api_instance.create_batch(merchant_id, opts)
  p result
rescue Shippo::ApiError => e
  puts "Exception when calling BatchesApi->create_batch: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **merchant_id** | **String**| description: Object ID of the merchant | 
 **body** | [**BatchToCreate**](BatchToCreate.md)| Batch details. | [optional] 
 **shippo_api_version** | **String**| description: String used to pick a non-default api version to use | [optional] 

### Return type

[**Batch**](Batch.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **get_batch**
> Batch get_batch(batch_id, merchant_id, opts)

Retrieve a batch

Retrieve an existing batch object.

### Example
```ruby
# load the gem
require 'shippo'
# setup authorization
Shippo.configure do |config|
  # Configure API key authorization: APIKeyHeader
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = Shippo::BatchesApi.new
batch_id = 'batch_id_example' # String | description: Object ID of the batch
merchant_id = 'merchant_id_example' # String | description: Object ID of the merchant
opts = { 
  shippo_api_version: 'shippo_api_version_example' # String | description: String used to pick a non-default api version to use
}

begin
  #Retrieve a batch
  result = api_instance.get_batch(batch_id, merchant_id, opts)
  p result
rescue Shippo::ApiError => e
  puts "Exception when calling BatchesApi->get_batch: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **batch_id** | **String**| description: Object ID of the batch | 
 **merchant_id** | **String**| description: Object ID of the merchant | 
 **shippo_api_version** | **String**| description: String used to pick a non-default api version to use | [optional] 

### Return type

[**Batch**](Batch.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **purchase_batch**
> Batch purchase_batch(batch_id, merchant_id, opts)

Purchase a batch

Purchase an existing batch with an status of VALID. Once you send a POST request to the purchase endpoint the batch status will change to PURCHASING. When all the shipments are purchased, the status will change to PURCHASED and you will receive a batch_purchased webhook indicating that the batch has been purchased

### Example
```ruby
# load the gem
require 'shippo'
# setup authorization
Shippo.configure do |config|
  # Configure API key authorization: APIKeyHeader
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = Shippo::BatchesApi.new
batch_id = 'batch_id_example' # String | description: Object ID of the batch
merchant_id = 'merchant_id_example' # String | description: Object ID of the merchant
opts = { 
  shippo_api_version: 'shippo_api_version_example' # String | description: String used to pick a non-default api version to use
}

begin
  #Purchase a batch
  result = api_instance.purchase_batch(batch_id, merchant_id, opts)
  p result
rescue Shippo::ApiError => e
  puts "Exception when calling BatchesApi->purchase_batch: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **batch_id** | **String**| description: Object ID of the batch | 
 **merchant_id** | **String**| description: Object ID of the merchant | 
 **shippo_api_version** | **String**| description: String used to pick a non-default api version to use | [optional] 

### Return type

[**Batch**](Batch.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **remove_shipments_from_batch**
> Batch remove_shipments_from_batch(batch_idmerchant_id, opts)

Remove shipments from a batch

Remove batch shipments from an existing batch.

### Example
```ruby
# load the gem
require 'shippo'
# setup authorization
Shippo.configure do |config|
  # Configure API key authorization: APIKeyHeader
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = Shippo::BatchesApi.new
batch_id = 'batch_id_example' # String | description: Object ID of the batch
merchant_id = 'merchant_id_example' # String | description: Object ID of the merchant
opts = { 
  body: ['body_example'] # Array<String> | Array of shipments object ids to remove from the batch
  shippo_api_version: 'shippo_api_version_example' # String | description: String used to pick a non-default api version to use
}

begin
  #Remove shipments from a batch
  result = api_instance.remove_shipments_from_batch(batch_idmerchant_id, opts)
  p result
rescue Shippo::ApiError => e
  puts "Exception when calling BatchesApi->remove_shipments_from_batch: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **batch_id** | **String**| description: Object ID of the batch | 
 **merchant_id** | **String**| description: Object ID of the merchant | 
 **body** | [**Array&lt;String&gt;**](String.md)| Array of shipments object ids to remove from the batch | [optional] 
 **shippo_api_version** | **String**| description: String used to pick a non-default api version to use | [optional] 

### Return type

[**Batch**](Batch.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



