# Shippo::Batch

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**batch_shipments** | [**BatchBatchShipments**](BatchBatchShipments.md) |  | 
**default_carrier_account** | **String** | ID of the Carrier Account object to use as the default for all shipments in this Batch. The carrier account can be changed on a per-shipment basis by changing the carrier_account in the corresponding BatchShipment object. | 
**default_servicelevel_token** | **String** | Token of the service level to use as the default for all shipments in this Batch. The servicelevel can be changed on a per-shipment basis by changing the servicelevel_token in the corresponding BatchShipment object. | 
**label_filetype** | **String** | Print format of the label. If empty, will use the default format. | [optional] 
**label_url** | **Array&lt;String&gt;** | An array of URLs each pointing to a merged file of 100 labels each | 
**metadata** | **String** | A string of up to 100 characters that can be filled with any additional information you want to attach to the object. | [optional] 
**object_created** | **String** | Date and time of Batch creation | 
**object_id** | **String** | Unique identifier of the given Batch object | 
**object_owner** | **String** | Username of the user who created the Address object. | 
**object_results** | [**BatchObjectResults**](BatchObjectResults.md) |  | 
**object_updated** | **String** | Date and time of last update to the Batch | 
**status** | **String** | Batches that are VALIDATING are being created and validated. VALID batches can be purchased, INVALID batches cannot be purchased, INVALID BatchShipments must be removed. Batches that are in the PURCHASING state are being purchased, PURCHASED batches are finished purchasing. | 
**test** | **BOOLEAN** |  | [optional] 

