# Shippo::TransactionsApi

All URIs are relative to *https://platform-api.goshippo.com/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_transaction**](TransactionsApi.md#create_transaction) | **POST** /merchants/{MerchantId}/transactions/ | Create a shipping label (rate)
[**get_transaction**](TransactionsApi.md#get_transaction) | **GET** /merchants/{MerchantId}/transactions/{TransactionId}/ | Retrieve a shipping label
[**list_transaction**](TransactionsApi.md#list_transaction) | **GET** /merchants/{MerchantId}/transactions/ | List all shipping labels

# **create_transaction**
> Transaction create_transaction(merchant_id, opts)

Create a shipping label (rate)

Creates a new transaction object and purchases the shipping label for the provided rate.

### Example
```ruby
# load the gem
require 'shippo'
# setup authorization
Shippo.configure do |config|
  # Configure API key authorization: APIKeyHeader
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = Shippo::TransactionsApi.new
merchant_id = 'merchant_id_example' # String | Merchant ID
opts = { 
  body: Shippo::TransactionRequestBody.new # TransactionRequestBody | Examples.
  shippo_api_version: 'shippo_api_version_example' # String | SHIPPO-API-VERSION
}

begin
  #Create a shipping label (rate)
  result = api_instance.create_transaction(merchant_id, opts)
  p result
rescue Shippo::ApiError => e
  puts "Exception when calling TransactionsApi->create_transaction: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **merchant_id** | **String**| Merchant ID | 
 **body** | [**TransactionRequestBody**](TransactionRequestBody.md)| Examples. | [optional] 
 **shippo_api_version** | **String**| SHIPPO-API-VERSION | [optional] 

### Return type

[**Transaction**](Transaction.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **get_transaction**
> Transaction get_transaction(merchant_id, transaction_id, opts)

Retrieve a shipping label

Retrieve an existing transaction by object id

### Example
```ruby
# load the gem
require 'shippo'
# setup authorization
Shippo.configure do |config|
  # Configure API key authorization: APIKeyHeader
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = Shippo::TransactionsApi.new
merchant_id = 'merchant_id_example' # String | description: Object ID of the merchant
transaction_id = 'transaction_id_example' # String | description: Object ID of the transaction to update
opts = { 
  shippo_api_version: 'shippo_api_version_example' # String | description: String used to pick a non-default api version to use
}

begin
  #Retrieve a shipping label
  result = api_instance.get_transaction(merchant_id, transaction_id, opts)
  p result
rescue Shippo::ApiError => e
  puts "Exception when calling TransactionsApi->get_transaction: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **merchant_id** | **String**| description: Object ID of the merchant | 
 **transaction_id** | **String**| description: Object ID of the transaction to update | 
 **shippo_api_version** | **String**| description: String used to pick a non-default api version to use | [optional] 

### Return type

[**Transaction**](Transaction.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **list_transaction**
> PaginatedTransactionResponse list_transaction(merchant_id, opts)

List all shipping labels

List all transaction objects

### Example
```ruby
# load the gem
require 'shippo'
# setup authorization
Shippo.configure do |config|
  # Configure API key authorization: APIKeyHeader
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = Shippo::TransactionsApi.new
merchant_id = 'merchant_id_example' # String | description: Object ID of the merchant
opts = { 
  shippo_api_version: 'shippo_api_version_example', # String | description: String used to pick a non-default api version to use
  page: 1, # Integer | description: The page number you want to select
  results: 25 # Integer | description: The number of results to return per page (max 100)
}

begin
  #List all shipping labels
  result = api_instance.list_transaction(merchant_id, opts)
  p result
rescue Shippo::ApiError => e
  puts "Exception when calling TransactionsApi->list_transaction: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **merchant_id** | **String**| description: Object ID of the merchant | 
 **shippo_api_version** | **String**| description: String used to pick a non-default api version to use | [optional] 
 **page** | **Integer**| description: The page number you want to select | [optional] [default to 1]
 **results** | **Integer**| description: The number of results to return per page (max 100) | [optional] [default to 25]

### Return type

[**PaginatedTransactionResponse**](PaginatedTransactionResponse.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



