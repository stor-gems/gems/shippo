# Shippo::UPSCreateNewOwnAccountParameters

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**billing_address_city** | **String** |  | 
**billing_address_country_iso2** | **String** |  | 
**billing_address_state** | **String** |  | 
**billing_address_street1** | **String** |  | 
**billing_address_street2** | **String** | Empty string acceptable for billing_address_street2 | [optional] 
**billing_address_zip** | **String** |  | 
**company** | **String** | Company name. Full name is acceptable in this field if the user has no company name | 
**email** | **String** |  | 
**full_name** | **String** |  | 
**phone** | **String** |  | 
**pickup_address_city** | **String** | User&#x27;s pickup address city. Only required if pickup_address_same_as_billing_address is false | [optional] 
**pickup_address_country_iso2** | **String** | User&#x27;s pickup street 1. Only required if pickup_address_same_as_billing_address is false | [optional] 
**pickup_address_same_as_billing_address** | **BOOLEAN** |  | 
**pickup_address_state** | **String** | User&#x27;s pickup address state. Only required if pickup_address_same_as_billing_address is false | [optional] 
**pickup_address_street1** | **String** | User&#x27;s pickup address street 1. Only required if pickup_address_same_as_billing_address is false | [optional] 
**pickup_address_street2** | **String** | User&#x27;s pickup street 2. Only used if pickup_address_same_as_billing_address is false, empty string acceptable | [optional] 
**pickup_address_zip** | **String** | User&#x27;s pickup address zip code. Only required if pickup_address_same_as_billing_address is false | [optional] 
**ups_agreements** | **BOOLEAN** | Whether the user agrees to the UPS terms and conditions or not. Error 400 will be returned if passed in as false | 

