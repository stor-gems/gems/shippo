# Shippo::ErrorMessage

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**shipment** | **Array&lt;Hash&lt;String, Array&lt;String&gt;&gt;&gt;** |  | [optional] 

