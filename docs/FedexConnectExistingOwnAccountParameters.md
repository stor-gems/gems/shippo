# Shippo::FedexConnectExistingOwnAccountParameters

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**first_name** | **String** |  | [optional] 
**from_address_city** | **String** |  | [optional] 
**from_address_country_iso2** | **String** |  | [optional] 
**from_address_st** | **String** |  | [optional] 
**from_address_state** | **String** |  | [optional] 
**from_address_zip** | **String** |  | [optional] 
**last_name** | **String** |  | [optional] 
**meter** | **String** |  | 
**phone_number** | **String** |  | [optional] 
**smartpost_id** | **String** |  | [optional] 

