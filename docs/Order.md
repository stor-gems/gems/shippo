# Shippo::Order

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**currency** | **String** | Currency of the total_price and total_tax amounts. Required if total_price is provided | [optional] 
**from_address** | [**BaseAddressStruct**](BaseAddressStruct.md) |  | [optional] 
**line_items** | [**Array&lt;LineItem&gt;**](LineItem.md) | Array of line item objects representing the items in this order. All objects will be returned expanded by default. | [optional] 
**notes** | **String** | Custom buyer- or seller-provided notes about the order. | [optional] 
**object_id** | **String** |  | [optional] 
**object_owner** | **String** |  | [optional] 
**order_number** | **String** | An alphanumeric identifier for the order used by the seller/buyer. This identifier doesn&#x27;t need to be unique. | [optional] 
**order_status** | **String** | Current state of the order. | [optional] 
**placed_at** | **String** | Date and time when the order was placed. This datetime can be different from the datetime of the order object creation on Shippo. | 
**shipping_cost** | **String** | Amount paid by the buyer for shipping. This amount can be different from the price the seller will actually pay for shipping. | [optional] 
**shipping_cost_currency** | **String** | Currency of the shipping_cost amount. Required if shipping_cost is provided | [optional] 
**shipping_method** | **String** | Shipping method (carrier + service or other free text description) chosen by the buyer. This value can be different from the shipping method the seller will actually choose. | [optional] 
**subtotal_price** | **String** |  | [optional] 
**to_address** | [**BaseAddressStruct**](BaseAddressStruct.md) |  | 
**total_price** | **String** | Total amount paid by the buyer for this order. | [optional] 
**total_tax** | **String** | Total tax amount paid by the buyer for this order. | [optional] 
**transactions** | **Array&lt;String&gt;** |  | [optional] 
**weight** | **String** |  | [optional] 
**weight_unit** | **String** |  | [optional] 

