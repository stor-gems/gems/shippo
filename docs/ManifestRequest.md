# Shippo::ManifestRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address_from** | [**BaseAddressStruct**](BaseAddressStruct.md) |  | 
**async** | **BOOLEAN** |  | [optional] 
**carrier_account** | **String** |  | 
**shipment_date** | **String** |  | 
**transactions** | **Array&lt;String&gt;** |  | [optional] 

