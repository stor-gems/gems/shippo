# Shippo::PaginatedAddressResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_next** | **String** |  | [optional] 
**previous** | **String** |  | [optional] 
**results** | [**Array&lt;Address&gt;**](Address.md) |  | [optional] 

