=begin
#Platform external API.

#Documentation of our Platform external API.

OpenAPI spec version: 1.0.0

Generated by: https://github.com/swagger-api/swagger-codegen.git
Swagger Codegen version: 3.0.34
=end

module Shippo
  VERSION = '1.0.0'
end
