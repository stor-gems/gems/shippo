=begin
#Platform external API.

#Documentation of our Platform external API.

OpenAPI spec version: 1.0.0

Generated by: https://github.com/swagger-api/swagger-codegen.git
Swagger Codegen version: 3.0.34
=end

module Shippo
  class CustomsDeclarationsApi
    attr_accessor :api_client

    def initialize(api_client = ApiClient.default)
      @api_client = api_client
    end
    # Create a new customs declaration
    # Creates a new customs declaration object
    # @param merchant_id description: Object ID of the merchant
    # @param [Hash] opts the optional parameters
    # @option opts [CreateCustomsDeclaration] :body CustomsDeclaration details.
    # @option opts [String] :shippo_api_version description: String used to pick a non-default api version to use
    # @return [CustomsDeclaration]
    def create_customs_declaration(merchant_id, opts = {})
      data, _status_code, _headers = create_customs_declaration_with_http_info(merchant_id, opts)
      data
    end

    # Create a new customs declaration
    # Creates a new customs declaration object
    # @param merchant_id description: Object ID of the merchant
    # @param [Hash] opts the optional parameters
    # @option opts [CreateCustomsDeclaration] :body CustomsDeclaration details.
    # @option opts [String] :shippo_api_version description: String used to pick a non-default api version to use
    # @return [Array<(CustomsDeclaration, Integer, Hash)>] CustomsDeclaration data, response status code and response headers
    def create_customs_declaration_with_http_info(merchant_id, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: CustomsDeclarationsApi.create_customs_declaration ...'
      end
      # verify the required parameter 'merchant_id' is set
      if @api_client.config.client_side_validation && merchant_id.nil?
        fail ArgumentError, "Missing the required parameter 'merchant_id' when calling CustomsDeclarationsApi.create_customs_declaration"
      end
      # resource path
      local_var_path = '/merchants/{MerchantId}/customs/declarations/'.sub('{' + 'MerchantId' + '}', merchant_id.to_s)

      # query parameters
      query_params = opts[:query_params] || {}

      # header parameters
      header_params = opts[:header_params] || {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json'])
      # HTTP header 'Content-Type'
      header_params['Content-Type'] = @api_client.select_header_content_type(['application/json'])
      header_params[:'SHIPPO-API-VERSION'] = opts[:'shippo_api_version'] if !opts[:'shippo_api_version'].nil?

      # form parameters
      form_params = opts[:form_params] || {}

      # http body (model)
      post_body = opts[:body] || @api_client.object_to_http_body(opts[:'body']) 

      return_type = opts[:return_type] || 'CustomsDeclaration' 

      auth_names = opts[:auth_names] || ['APIKeyHeader']
      data, status_code, headers = @api_client.call_api(:POST, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => return_type)

      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: CustomsDeclarationsApi#create_customs_declaration\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end
    # List all customs declarations
    # List all customs declaration objects
    # @param merchant_id description: Object ID of the merchant
    # @param [Hash] opts the optional parameters
    # @option opts [String] :shippo_api_version description: String used to pick a non-default api version to use
    # @option opts [Integer] :page description: The page number you want to select (default to 1)
    # @option opts [Integer] :results description: The number of results to return per page (max 100) (default to 25)
    # @return [PaginatedCustomsDeclarationsResponse]
    def list_customs_declarations(merchant_id, opts = {})
      data, _status_code, _headers = list_customs_declarations_with_http_info(merchant_id, opts)
      data
    end

    # List all customs declarations
    # List all customs declaration objects
    # @param merchant_id description: Object ID of the merchant
    # @param [Hash] opts the optional parameters
    # @option opts [String] :shippo_api_version description: String used to pick a non-default api version to use
    # @option opts [Integer] :page description: The page number you want to select
    # @option opts [Integer] :results description: The number of results to return per page (max 100)
    # @return [Array<(PaginatedCustomsDeclarationsResponse, Integer, Hash)>] PaginatedCustomsDeclarationsResponse data, response status code and response headers
    def list_customs_declarations_with_http_info(merchant_id, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: CustomsDeclarationsApi.list_customs_declarations ...'
      end
      # verify the required parameter 'merchant_id' is set
      if @api_client.config.client_side_validation && merchant_id.nil?
        fail ArgumentError, "Missing the required parameter 'merchant_id' when calling CustomsDeclarationsApi.list_customs_declarations"
      end
      # resource path
      local_var_path = '/merchants/{MerchantId}/customs/declarations/'.sub('{' + 'MerchantId' + '}', merchant_id.to_s)

      # query parameters
      query_params = opts[:query_params] || {}
      query_params[:'page'] = opts[:'page'] if !opts[:'page'].nil?
      query_params[:'results'] = opts[:'results'] if !opts[:'results'].nil?

      # header parameters
      header_params = opts[:header_params] || {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json'])
      header_params[:'SHIPPO-API-VERSION'] = opts[:'shippo_api_version'] if !opts[:'shippo_api_version'].nil?

      # form parameters
      form_params = opts[:form_params] || {}

      # http body (model)
      post_body = opts[:body] 

      return_type = opts[:return_type] || 'PaginatedCustomsDeclarationsResponse' 

      auth_names = opts[:auth_names] || ['APIKeyHeader']
      data, status_code, headers = @api_client.call_api(:GET, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => return_type)

      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: CustomsDeclarationsApi#list_customs_declarations\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end
    # Retrieve a customs declaration
    # Retrieve an existing customs declaration by object id
    # @param customs_declaration_id description: Object ID of the customs declaration
    # @param merchant_id description: Object ID of the merchant
    # @param [Hash] opts the optional parameters
    # @option opts [String] :shippo_api_version description: String used to pick a non-default api version to use
    # @option opts [Integer] :page description: The page number you want to select (default to 1)
    # @return [CustomsDeclaration]
    def get_customs_declaration(customs_declaration_id, merchant_id, opts = {})
      data, _status_code, _headers = get_customs_declaration_with_http_info(customs_declaration_id, merchant_id, opts)
      data
    end

    # Retrieve a customs declaration
    # Retrieve an existing customs declaration by object id
    # @param customs_declaration_id description: Object ID of the customs declaration
    # @param merchant_id description: Object ID of the merchant
    # @param [Hash] opts the optional parameters
    # @option opts [String] :shippo_api_version description: String used to pick a non-default api version to use
    # @option opts [Integer] :page description: The page number you want to select
    # @return [Array<(CustomsDeclaration, Integer, Hash)>] CustomsDeclaration data, response status code and response headers
    def get_customs_declaration_with_http_info(customs_declaration_id, merchant_id, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: CustomsDeclarationsApi.get_customs_declaration ...'
      end
      # verify the required parameter 'customs_declaration_id' is set
      if @api_client.config.client_side_validation && customs_declaration_id.nil?
        fail ArgumentError, "Missing the required parameter 'customs_declaration_id' when calling CustomsDeclarationsApi.get_customs_declaration"
      end
      # verify the required parameter 'merchant_id' is set
      if @api_client.config.client_side_validation && merchant_id.nil?
        fail ArgumentError, "Missing the required parameter 'merchant_id' when calling CustomsDeclarationsApi.get_customs_declaration"
      end
      # resource path
      local_var_path = '/merchants/{MerchantId}/customs/declarations/{CustomsDeclarationId}/'.sub('{' + 'CustomsDeclarationId' + '}', customs_declaration_id.to_s).sub('{' + 'MerchantId' + '}', merchant_id.to_s)

      # query parameters
      query_params = opts[:query_params] || {}
      query_params[:'page'] = opts[:'page'] if !opts[:'page'].nil?

      # header parameters
      header_params = opts[:header_params] || {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json'])
      header_params[:'SHIPPO-API-VERSION'] = opts[:'shippo_api_version'] if !opts[:'shippo_api_version'].nil?

      # form parameters
      form_params = opts[:form_params] || {}

      # http body (model)
      post_body = opts[:body] 

      return_type = opts[:return_type] || 'CustomsDeclaration' 

      auth_names = opts[:auth_names] || ['APIKeyHeader']
      data, status_code, headers = @api_client.call_api(:GET, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => return_type)

      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: CustomsDeclarationsApi#get_customs_declaration\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end
  end
end
