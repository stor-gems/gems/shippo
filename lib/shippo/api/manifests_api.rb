=begin
#Platform external API.

#Documentation of our Platform external API.

OpenAPI spec version: 1.0.0

Generated by: https://github.com/swagger-api/swagger-codegen.git
Swagger Codegen version: 3.0.34
=end

module Shippo
  class ManifestsApi
    attr_accessor :api_client

    def initialize(api_client = ApiClient.default)
      @api_client = api_client
    end
    # Create a new manifest
    # Creates a new manifest object.
    # @param merchant_id description: Object ID of the merchant
    # @param [Hash] opts the optional parameters
    # @option opts [ManifestRequest] :body Manifest details and contact info.
    # @option opts [String] :shippo_api_version description: String used to pick a non-default api version to use
    # @return [Manifest]
    def create_manifest(merchant_id, opts = {})
      data, _status_code, _headers = create_manifest_with_http_info(merchant_id, opts)
      data
    end

    # Create a new manifest
    # Creates a new manifest object.
    # @param merchant_id description: Object ID of the merchant
    # @param [Hash] opts the optional parameters
    # @option opts [ManifestRequest] :body Manifest details and contact info.
    # @option opts [String] :shippo_api_version description: String used to pick a non-default api version to use
    # @return [Array<(Manifest, Integer, Hash)>] Manifest data, response status code and response headers
    def create_manifest_with_http_info(merchant_id, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: ManifestsApi.create_manifest ...'
      end
      # verify the required parameter 'merchant_id' is set
      if @api_client.config.client_side_validation && merchant_id.nil?
        fail ArgumentError, "Missing the required parameter 'merchant_id' when calling ManifestsApi.create_manifest"
      end
      # resource path
      local_var_path = '/merchants/{MerchantId}/manifests/'.sub('{' + 'MerchantId' + '}', merchant_id.to_s)

      # query parameters
      query_params = opts[:query_params] || {}

      # header parameters
      header_params = opts[:header_params] || {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json'])
      # HTTP header 'Content-Type'
      header_params['Content-Type'] = @api_client.select_header_content_type(['application/json'])
      header_params[:'SHIPPO-API-VERSION'] = opts[:'shippo_api_version'] if !opts[:'shippo_api_version'].nil?

      # form parameters
      form_params = opts[:form_params] || {}

      # http body (model)
      post_body = opts[:body] || @api_client.object_to_http_body(opts[:'body']) 

      return_type = opts[:return_type] || 'Manifest' 

      auth_names = opts[:auth_names] || ['APIKeyHeader']
      data, status_code, headers = @api_client.call_api(:POST, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => return_type)

      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: ManifestsApi#create_manifest\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end
    # Retrieve a manifest
    # Retrieve an existing manifest by object id.
    # @param merchant_id description: Object ID of the merchant
    # @param manifest_id description: Object ID of the manifest to update
    # @param [Hash] opts the optional parameters
    # @option opts [String] :shippo_api_version description: String used to pick a non-default api version to use
    # @return [Manifest]
    def get_manifest(merchant_id, manifest_id, opts = {})
      data, _status_code, _headers = get_manifest_with_http_info(merchant_id, manifest_id, opts)
      data
    end

    # Retrieve a manifest
    # Retrieve an existing manifest by object id.
    # @param merchant_id description: Object ID of the merchant
    # @param manifest_id description: Object ID of the manifest to update
    # @param [Hash] opts the optional parameters
    # @option opts [String] :shippo_api_version description: String used to pick a non-default api version to use
    # @return [Array<(Manifest, Integer, Hash)>] Manifest data, response status code and response headers
    def get_manifest_with_http_info(merchant_id, manifest_id, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: ManifestsApi.get_manifest ...'
      end
      # verify the required parameter 'merchant_id' is set
      if @api_client.config.client_side_validation && merchant_id.nil?
        fail ArgumentError, "Missing the required parameter 'merchant_id' when calling ManifestsApi.get_manifest"
      end
      # verify the required parameter 'manifest_id' is set
      if @api_client.config.client_side_validation && manifest_id.nil?
        fail ArgumentError, "Missing the required parameter 'manifest_id' when calling ManifestsApi.get_manifest"
      end
      # resource path
      local_var_path = '/merchants/{MerchantId}/manifests/{ManifestId}/'.sub('{' + 'MerchantId' + '}', merchant_id.to_s).sub('{' + 'ManifestId' + '}', manifest_id.to_s)

      # query parameters
      query_params = opts[:query_params] || {}

      # header parameters
      header_params = opts[:header_params] || {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json'])
      header_params[:'SHIPPO-API-VERSION'] = opts[:'shippo_api_version'] if !opts[:'shippo_api_version'].nil?

      # form parameters
      form_params = opts[:form_params] || {}

      # http body (model)
      post_body = opts[:body] 

      return_type = opts[:return_type] || 'Manifest' 

      auth_names = opts[:auth_names] || ['APIKeyHeader']
      data, status_code, headers = @api_client.call_api(:GET, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => return_type)

      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: ManifestsApi#get_manifest\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end
    # List all manifests
    # List all manifest objects.
    # @param merchant_id description: Object ID of the merchant
    # @param [Hash] opts the optional parameters
    # @option opts [String] :shippo_api_version description: String used to pick a non-default api version to use
    # @option opts [Integer] :page description: The page number you want to select (default to 1)
    # @option opts [Integer] :results description: The number of results to return per page (max 100) (default to 25)
    # @return [PaginatedManifestResponse]
    def list_manifest(merchant_id, opts = {})
      data, _status_code, _headers = list_manifest_with_http_info(merchant_id, opts)
      data
    end

    # List all manifests
    # List all manifest objects.
    # @param merchant_id description: Object ID of the merchant
    # @param [Hash] opts the optional parameters
    # @option opts [String] :shippo_api_version description: String used to pick a non-default api version to use
    # @option opts [Integer] :page description: The page number you want to select
    # @option opts [Integer] :results description: The number of results to return per page (max 100)
    # @return [Array<(PaginatedManifestResponse, Integer, Hash)>] PaginatedManifestResponse data, response status code and response headers
    def list_manifest_with_http_info(merchant_id, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: ManifestsApi.list_manifest ...'
      end
      # verify the required parameter 'merchant_id' is set
      if @api_client.config.client_side_validation && merchant_id.nil?
        fail ArgumentError, "Missing the required parameter 'merchant_id' when calling ManifestsApi.list_manifest"
      end
      # resource path
      local_var_path = '/merchants/{MerchantId}/manifests/'.sub('{' + 'MerchantId' + '}', merchant_id.to_s)

      # query parameters
      query_params = opts[:query_params] || {}
      query_params[:'page'] = opts[:'page'] if !opts[:'page'].nil?
      query_params[:'results'] = opts[:'results'] if !opts[:'results'].nil?

      # header parameters
      header_params = opts[:header_params] || {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json'])
      header_params[:'SHIPPO-API-VERSION'] = opts[:'shippo_api_version'] if !opts[:'shippo_api_version'].nil?

      # form parameters
      form_params = opts[:form_params] || {}

      # http body (model)
      post_body = opts[:body] 

      return_type = opts[:return_type] || 'PaginatedManifestResponse' 

      auth_names = opts[:auth_names] || ['APIKeyHeader']
      data, status_code, headers = @api_client.call_api(:GET, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => return_type)

      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: ManifestsApi#list_manifest\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end
  end
end
