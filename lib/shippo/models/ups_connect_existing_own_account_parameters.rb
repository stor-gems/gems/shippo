=begin
#Platform external API.

#Documentation of our Platform external API.

OpenAPI spec version: 1.0.0

Generated by: https://github.com/swagger-api/swagger-codegen.git
Swagger Codegen version: 3.0.34
=end

require 'date'

module Shippo
  class UPSConnectExistingOwnAccountParameters
    # The UPS account number
    attr_accessor :account_number

    # Only required if has_invoice is true. Country associated with the account that issued the invoice
    attr_accessor :aia_country_iso2

    attr_accessor :billing_address_city

    attr_accessor :billing_address_country_iso2

    attr_accessor :billing_address_state

    attr_accessor :billing_address_street1

    # Empty string acceptable for billing_address_street2
    attr_accessor :billing_address_street2

    attr_accessor :billing_address_zip

    attr_accessor :collec_country_iso2

    # Zip code of the collection/pickup address
    attr_accessor :collec_zip

    # Company name. Full name is acceptable in this field if the user has no company name
    attr_accessor :company

    # Only required if has_invoice is true. 3-letter currency code associated with invoice_value
    attr_accessor :currency_code

    attr_accessor :email

    attr_accessor :full_name

    # true if user has been issued a UPS invoice within the past 90 days for the US or Canada; and 45 days for any other countries. User can use data from any of the last 3 invoices
    attr_accessor :has_invoice

    # Only required if aia_country_iso2 is US and has_invoice is true.
    attr_accessor :invoice_controlid

    # Only required if has_invoice is true. Date the invoice was issued. yyyymmdd format
    attr_accessor :invoice_date

    attr_accessor :invoice_number

    # Only required if has_invoice is true. Max 16 digits before decimal and 2 digits after decimal
    attr_accessor :invoice_value

    attr_accessor :phone

    # User's title, e.g. including but not limited to Manager, Doctor, Artist, Engineer, Mr, Ms, Mrs, Mx
    attr_accessor :title

    # Whether the user agrees to the UPS terms and conditions or not. Error 400 will be returned if passed in as false
    attr_accessor :ups_agreements

    # Attribute mapping from ruby-style variable name to JSON key.
    def self.attribute_map
      {
        :'account_number' => :'account_number',
        :'aia_country_iso2' => :'aia_country_iso2',
        :'billing_address_city' => :'billing_address_city',
        :'billing_address_country_iso2' => :'billing_address_country_iso2',
        :'billing_address_state' => :'billing_address_state',
        :'billing_address_street1' => :'billing_address_street1',
        :'billing_address_street2' => :'billing_address_street2',
        :'billing_address_zip' => :'billing_address_zip',
        :'collec_country_iso2' => :'collec_country_iso2',
        :'collec_zip' => :'collec_zip',
        :'company' => :'company',
        :'currency_code' => :'currency_code',
        :'email' => :'email',
        :'full_name' => :'full_name',
        :'has_invoice' => :'has_invoice',
        :'invoice_controlid' => :'invoice_controlid',
        :'invoice_date' => :'invoice_date',
        :'invoice_number' => :'invoice_number',
        :'invoice_value' => :'invoice_value',
        :'phone' => :'phone',
        :'title' => :'title',
        :'ups_agreements' => :'ups_agreements'
      }
    end

    # Attribute type mapping.
    def self.openapi_types
      {
        :'account_number' => :'Object',
        :'aia_country_iso2' => :'Object',
        :'billing_address_city' => :'Object',
        :'billing_address_country_iso2' => :'Object',
        :'billing_address_state' => :'Object',
        :'billing_address_street1' => :'Object',
        :'billing_address_street2' => :'Object',
        :'billing_address_zip' => :'Object',
        :'collec_country_iso2' => :'Object',
        :'collec_zip' => :'Object',
        :'company' => :'Object',
        :'currency_code' => :'Object',
        :'email' => :'Object',
        :'full_name' => :'Object',
        :'has_invoice' => :'Object',
        :'invoice_controlid' => :'Object',
        :'invoice_date' => :'Object',
        :'invoice_number' => :'Object',
        :'invoice_value' => :'Object',
        :'phone' => :'Object',
        :'title' => :'Object',
        :'ups_agreements' => :'Object'
      }
    end

    # List of attributes with nullable: true
    def self.openapi_nullable
      Set.new([
      ])
    end
  
    # Initializes the object
    # @param [Hash] attributes Model attributes in the form of hash
    def initialize(attributes = {})
      if (!attributes.is_a?(Hash))
        fail ArgumentError, "The input argument (attributes) must be a hash in `Shippo::UPSConnectExistingOwnAccountParameters` initialize method"
      end

      # check to see if the attribute exists and convert string to symbol for hash key
      attributes = attributes.each_with_object({}) { |(k, v), h|
        if (!self.class.attribute_map.key?(k.to_sym))
          fail ArgumentError, "`#{k}` is not a valid attribute in `Shippo::UPSConnectExistingOwnAccountParameters`. Please check the name to make sure it's valid. List of attributes: " + self.class.attribute_map.keys.inspect
        end
        h[k.to_sym] = v
      }

      if attributes.key?(:'account_number')
        self.account_number = attributes[:'account_number']
      end

      if attributes.key?(:'aia_country_iso2')
        self.aia_country_iso2 = attributes[:'aia_country_iso2']
      end

      if attributes.key?(:'billing_address_city')
        self.billing_address_city = attributes[:'billing_address_city']
      end

      if attributes.key?(:'billing_address_country_iso2')
        self.billing_address_country_iso2 = attributes[:'billing_address_country_iso2']
      end

      if attributes.key?(:'billing_address_state')
        self.billing_address_state = attributes[:'billing_address_state']
      end

      if attributes.key?(:'billing_address_street1')
        self.billing_address_street1 = attributes[:'billing_address_street1']
      end

      if attributes.key?(:'billing_address_street2')
        self.billing_address_street2 = attributes[:'billing_address_street2']
      end

      if attributes.key?(:'billing_address_zip')
        self.billing_address_zip = attributes[:'billing_address_zip']
      end

      if attributes.key?(:'collec_country_iso2')
        self.collec_country_iso2 = attributes[:'collec_country_iso2']
      end

      if attributes.key?(:'collec_zip')
        self.collec_zip = attributes[:'collec_zip']
      end

      if attributes.key?(:'company')
        self.company = attributes[:'company']
      end

      if attributes.key?(:'currency_code')
        self.currency_code = attributes[:'currency_code']
      end

      if attributes.key?(:'email')
        self.email = attributes[:'email']
      end

      if attributes.key?(:'full_name')
        self.full_name = attributes[:'full_name']
      end

      if attributes.key?(:'has_invoice')
        self.has_invoice = attributes[:'has_invoice']
      end

      if attributes.key?(:'invoice_controlid')
        self.invoice_controlid = attributes[:'invoice_controlid']
      end

      if attributes.key?(:'invoice_date')
        self.invoice_date = attributes[:'invoice_date']
      end

      if attributes.key?(:'invoice_number')
        self.invoice_number = attributes[:'invoice_number']
      end

      if attributes.key?(:'invoice_value')
        self.invoice_value = attributes[:'invoice_value']
      end

      if attributes.key?(:'phone')
        self.phone = attributes[:'phone']
      end

      if attributes.key?(:'title')
        self.title = attributes[:'title']
      end

      if attributes.key?(:'ups_agreements')
        self.ups_agreements = attributes[:'ups_agreements']
      end
    end

    # Show invalid properties with the reasons. Usually used together with valid?
    # @return Array for valid properties with the reasons
    def list_invalid_properties
      invalid_properties = Array.new
      if @account_number.nil?
        invalid_properties.push('invalid value for "account_number", account_number cannot be nil.')
      end

      if @billing_address_city.nil?
        invalid_properties.push('invalid value for "billing_address_city", billing_address_city cannot be nil.')
      end

      if @billing_address_country_iso2.nil?
        invalid_properties.push('invalid value for "billing_address_country_iso2", billing_address_country_iso2 cannot be nil.')
      end

      if @billing_address_state.nil?
        invalid_properties.push('invalid value for "billing_address_state", billing_address_state cannot be nil.')
      end

      if @billing_address_street1.nil?
        invalid_properties.push('invalid value for "billing_address_street1", billing_address_street1 cannot be nil.')
      end

      if @billing_address_zip.nil?
        invalid_properties.push('invalid value for "billing_address_zip", billing_address_zip cannot be nil.')
      end

      if @collec_country_iso2.nil?
        invalid_properties.push('invalid value for "collec_country_iso2", collec_country_iso2 cannot be nil.')
      end

      if @collec_zip.nil?
        invalid_properties.push('invalid value for "collec_zip", collec_zip cannot be nil.')
      end

      if @company.nil?
        invalid_properties.push('invalid value for "company", company cannot be nil.')
      end

      if @email.nil?
        invalid_properties.push('invalid value for "email", email cannot be nil.')
      end

      if @full_name.nil?
        invalid_properties.push('invalid value for "full_name", full_name cannot be nil.')
      end

      if @has_invoice.nil?
        invalid_properties.push('invalid value for "has_invoice", has_invoice cannot be nil.')
      end

      if @phone.nil?
        invalid_properties.push('invalid value for "phone", phone cannot be nil.')
      end

      if @title.nil?
        invalid_properties.push('invalid value for "title", title cannot be nil.')
      end

      if @ups_agreements.nil?
        invalid_properties.push('invalid value for "ups_agreements", ups_agreements cannot be nil.')
      end

      invalid_properties
    end

    # Check to see if the all the properties in the model are valid
    # @return true if the model is valid
    def valid?
      return false if @account_number.nil?
      return false if @billing_address_city.nil?
      return false if @billing_address_country_iso2.nil?
      return false if @billing_address_state.nil?
      return false if @billing_address_street1.nil?
      return false if @billing_address_zip.nil?
      return false if @collec_country_iso2.nil?
      return false if @collec_zip.nil?
      return false if @company.nil?
      return false if @email.nil?
      return false if @full_name.nil?
      return false if @has_invoice.nil?
      return false if @phone.nil?
      return false if @title.nil?
      return false if @ups_agreements.nil?
      true
    end

    # Checks equality by comparing each attribute.
    # @param [Object] Object to be compared
    def ==(o)
      return true if self.equal?(o)
      self.class == o.class &&
          account_number == o.account_number &&
          aia_country_iso2 == o.aia_country_iso2 &&
          billing_address_city == o.billing_address_city &&
          billing_address_country_iso2 == o.billing_address_country_iso2 &&
          billing_address_state == o.billing_address_state &&
          billing_address_street1 == o.billing_address_street1 &&
          billing_address_street2 == o.billing_address_street2 &&
          billing_address_zip == o.billing_address_zip &&
          collec_country_iso2 == o.collec_country_iso2 &&
          collec_zip == o.collec_zip &&
          company == o.company &&
          currency_code == o.currency_code &&
          email == o.email &&
          full_name == o.full_name &&
          has_invoice == o.has_invoice &&
          invoice_controlid == o.invoice_controlid &&
          invoice_date == o.invoice_date &&
          invoice_number == o.invoice_number &&
          invoice_value == o.invoice_value &&
          phone == o.phone &&
          title == o.title &&
          ups_agreements == o.ups_agreements
    end

    # @see the `==` method
    # @param [Object] Object to be compared
    def eql?(o)
      self == o
    end

    # Calculates hash code according to all attributes.
    # @return [Integer] Hash code
    def hash
      [account_number, aia_country_iso2, billing_address_city, billing_address_country_iso2, billing_address_state, billing_address_street1, billing_address_street2, billing_address_zip, collec_country_iso2, collec_zip, company, currency_code, email, full_name, has_invoice, invoice_controlid, invoice_date, invoice_number, invoice_value, phone, title, ups_agreements].hash
    end

    # Builds the object from hash
    # @param [Hash] attributes Model attributes in the form of hash
    # @return [Object] Returns the model itself
    def self.build_from_hash(attributes)
      new.build_from_hash(attributes)
    end

    # Builds the object from hash
    # @param [Hash] attributes Model attributes in the form of hash
    # @return [Object] Returns the model itself
    def build_from_hash(attributes)
      return nil unless attributes.is_a?(Hash)
      self.class.openapi_types.each_pair do |key, type|
        if type =~ /\AArray<(.*)>/i
          # check to ensure the input is an array given that the attribute
          # is documented as an array but the input is not
          if attributes[self.class.attribute_map[key]].is_a?(Array)
            self.send("#{key}=", attributes[self.class.attribute_map[key]].map { |v| _deserialize($1, v) })
          end
        elsif !attributes[self.class.attribute_map[key]].nil?
          self.send("#{key}=", _deserialize(type, attributes[self.class.attribute_map[key]]))
        elsif attributes[self.class.attribute_map[key]].nil? && self.class.openapi_nullable.include?(key)
          self.send("#{key}=", nil)
        end
      end

      self
    end

    # Deserializes the data based on type
    # @param string type Data type
    # @param string value Value to be deserialized
    # @return [Object] Deserialized data
    def _deserialize(type, value)
      case type.to_sym
      when :DateTime
        DateTime.parse(value)
      when :Date
        Date.parse(value)
      when :String
        value.to_s
      when :Integer
        value.to_i
      when :Float
        value.to_f
      when :Boolean
        if value.to_s =~ /\A(true|t|yes|y|1)\z/i
          true
        else
          false
        end
      when :Object
        # generic object (usually a Hash), return directly
        value
      when /\AArray<(?<inner_type>.+)>\z/
        inner_type = Regexp.last_match[:inner_type]
        value.map { |v| _deserialize(inner_type, v) }
      when /\AHash<(?<k_type>.+?), (?<v_type>.+)>\z/
        k_type = Regexp.last_match[:k_type]
        v_type = Regexp.last_match[:v_type]
        {}.tap do |hash|
          value.each do |k, v|
            hash[_deserialize(k_type, k)] = _deserialize(v_type, v)
          end
        end
      else # model
        Shippo.const_get(type).build_from_hash(value)
      end
    end

    # Returns the string representation of the object
    # @return [String] String presentation of the object
    def to_s
      to_hash.to_s
    end

    # to_body is an alias to to_hash (backward compatibility)
    # @return [Hash] Returns the object in the form of hash
    def to_body
      to_hash
    end

    # Returns the object in the form of hash
    # @return [Hash] Returns the object in the form of hash
    def to_hash
      hash = {}
      self.class.attribute_map.each_pair do |attr, param|
        value = self.send(attr)
        if value.nil?
          is_nullable = self.class.openapi_nullable.include?(attr)
          next if !is_nullable || (is_nullable && !instance_variable_defined?(:"@#{attr}"))
        end

        hash[param] = _to_hash(value)
      end
      hash
    end

    # Outputs non-array value in the form of hash
    # For object, use to_hash. Otherwise, just return the value
    # @param [Object] value Any valid value
    # @return [Hash] Returns the value in the form of hash
    def _to_hash(value)
      if value.is_a?(Array)
        value.compact.map { |v| _to_hash(v) }
      elsif value.is_a?(Hash)
        {}.tap do |hash|
          value.each { |k, v| hash[k] = _to_hash(v) }
        end
      elsif value.respond_to? :to_hash
        value.to_hash
      else
        value
      end
    end  end
end
