=begin
#Platform external API.

#Documentation of our Platform external API.

OpenAPI spec version: 1.0.0

Generated by: https://github.com/swagger-api/swagger-codegen.git
Swagger Codegen version: 3.0.34
=end

require 'spec_helper'
require 'json'
require 'date'

# Unit tests for Shippo::PlatformCarrierOwnAccountCreation400Response
# Automatically generated by swagger-codegen (github.com/swagger-api/swagger-codegen)
# Please update as you see appropriate
describe 'PlatformCarrierOwnAccountCreation400Response' do
  before do
    # run before each test
    @instance = Shippo::PlatformCarrierOwnAccountCreation400Response.new
  end

  after do
    # run after each test
  end

  describe 'test an instance of PlatformCarrierOwnAccountCreation400Response' do
    it 'should create an instance of PlatformCarrierOwnAccountCreation400Response' do
      expect(@instance).to be_instance_of(Shippo::PlatformCarrierOwnAccountCreation400Response)
    end
  end
  describe 'test attribute "detail"' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

end
